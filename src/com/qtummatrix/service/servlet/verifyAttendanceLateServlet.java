package com.qtummatrix.service.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;

import com.qtummatrix.dao.AttendanceDao;
import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;
import com.qtummatrix.entity.AttendanceBO;


@WebServlet("/verifyAttendanceLate")
public class verifyAttendanceLateServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		int attendanceId =Integer.parseInt(req.getParameter("attendanceId"));
		System.out.println("后端接到的ID"+attendanceId);
		AttendanceDao attendanceDao = new AttendanceDaoImpl();
		AttendanceBO abo = new AttendanceBO();
		List<AttendanceBO> aboList = new ArrayList<>();
		JSONArray jsonArray = new JSONArray();
		
		abo = attendanceDao.queryAttendance(attendanceId);
		System.out.println("123"+abo.toString());
		if(abo != null) {
			aboList.add(abo);
			String jsonStr = JSON.toJSONString(aboList);
			jsonArray = JSONArray.parseArray(jsonStr);
			System.out.println("查指定："+jsonArray);
			out.println(jsonArray);
		}
		
	}

	
}
