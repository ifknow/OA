package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

@WebServlet("/eidtUserServlet")
public class EidtUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        //编辑员工信息
        //获取员工id
        //调用数据库查询相应的字段，显示到前端

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int operationUserId = user.getId();

        //获取要编辑的员工Id
        int id = Integer.parseInt(request.getParameter("id"));
        UserDao userDao = new UserDaoImpl();
        LinkedList<Map> maps = userDao.showUserInfo(operationUserId, id);

        System.out.println("测试数据");
        for (Map map : maps) {
            System.out.println(map);
        }
        request.setAttribute("usermapss", maps);
        request.setAttribute("eidtUserMsg","信息修改成功");
        request.getRequestDispatcher("/eidtUser.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }
}
