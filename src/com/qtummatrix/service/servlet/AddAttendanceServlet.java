package com.qtummatrix.service.servlet;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.AttendanceDao;
import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;
import com.qtummatrix.entity.Attendance;
import com.qtummatrix.utils.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/addAttendanceServlet")
public class AddAttendanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");

        PrintWriter out = response.getWriter();

        //声明Attendance对象,存储前端传过来的考勤数据
        Attendance attendance = new Attendance();
        AttendanceDao attendanceDao = new AttendanceDaoImpl();


        //**验证输入是否为空将会在最后用前端验证
        int userId =Integer.parseInt(request.getParameter("userId"));
        String username =request.getParameter("username");
        String num1 = request.getParameter("leaveclass");
        String num2 = request.getParameter("leavetype");
        String leaveclass=null;
        String leavetype=null;
        if(num1.equals("0")) {
            leaveclass = "请假";
            if(num2.equals("0")) {
                leavetype = "调休";
            }else if(num2.equals("0")) {
                leavetype = "事假";
            }else if(num2.equals("0")) {
                leavetype = "病假";
            }else{
                leavetype = "产假";
            }
        }else if(num1.equals("1")) {
            leaveclass = "迟到/早退";
            if(num2.equals("0")) {
                leavetype = "迟到";
            }else {
                leavetype = "早退";
            }
        }else if(num1.equals("2")) {
            leaveclass = "出差";
            leavetype = "出差";
        }else if(num1.equals("3")) {
            leaveclass = "加班";
            leavetype = "加班";
        }else{
            leaveclass = "矿工";
            leavetype = "矿工";
        }
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        String aduitState = request.getParameter("aduitState");
        System.out.println("添加员工考勤信息："+username+","+leaveclass+","+leavetype+","+startTime+","+endTime+","+aduitState);



        Date starttime = DBUtil.StringtoDate(startTime);
        Date endtime = DBUtil.StringtoDate(endTime);

        System.out.println(username);

        JSONArray jsonArray = new JSONArray();
        List<String> str = new ArrayList<>();
        boolean b = attendanceDao.checkUsername(username);

        if(b) {
            boolean s = attendanceDao.addAttenDance(userId, leaveclass, leavetype, startTime, endTime, aduitState);
            if(s) {
                str.add("添加成功");
            }else {
                str.add("添加失败");
            }
        }else {
            str.add("员工信息不存在");
        }

        String jsonStr = JSON.toJSONString(str);
        jsonArray = JSONArray.parseArray(jsonStr);
        System.out.println(jsonArray);
        out.println(jsonArray);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
