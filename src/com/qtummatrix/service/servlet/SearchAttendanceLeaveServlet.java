package com.qtummatrix.service.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.AttendanceDao;

import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;
import com.qtummatrix.entity.AttendanceBO;


@WebServlet("/SearchAttendanceLeave")
public class SearchAttendanceLeaveServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("text/html");
		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd" );
		String userName = req.getParameter("username");
		String aduitState = req.getParameter("aduitState");
		String start = req.getParameter("startTime");
		String end = req.getParameter("endTime");
		
		Date startTime = null;
		if(!start.equals("")) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				startTime = new Date((format.parse(start)).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		Date endTime = null;
		if(!end.equals("")) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			try {
				endTime = new Date((format.parse(end)).getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		PrintWriter out = resp.getWriter();
		AttendanceDao attendanceDao = new AttendanceDaoImpl();
		JSONArray jsonArray = new JSONArray();
		List<AttendanceBO> aboList = new ArrayList<>();
		
		aboList = attendanceDao.queryAttendances(userName, startTime, endTime, aduitState);
		if(!aboList.isEmpty()) {
			String jsonStr = JSON.toJSONString(aboList);
			jsonArray = JSONArray.parseArray(jsonStr);
			System.out.println(jsonArray);
			out.println(jsonArray);
		}
	}
	
	

}
