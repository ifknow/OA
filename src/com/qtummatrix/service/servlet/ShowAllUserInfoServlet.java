package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

//显示所有员工信息
//处理分页请求
@WebServlet("/showAllUserInfoServlet")
public class ShowAllUserInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        UserDao userDao = new UserDaoImpl();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int operationUserId = user.getId();

        //当前页码
        String pageNumber = "1";
        //查看有多少条记录信息
        int personNum = userDao.selectTotalPerson();
//        System.out.println("共有记录数：" + personNum);
        //查看一共有多少页
        int totalPage = personNum % 6 == 0 ? personNum / 6 : personNum / 6 + 1;
        //当前页码是默认加载第一页，所以selectUserInfo参数为0，将查询出来0-6条记录
        LinkedList<Map> maps = userDao.selectUserInfo(operationUserId, 0);
        //显示数据给页面
        request.setAttribute("key_userList", maps);
        //当前页码
        request.setAttribute("pageNumber", pageNumber);
        //总共的页数
        request.setAttribute("pagetotalNumber", totalPage);
        //总共多少条记录
        request.setAttribute("totalNumber", personNum);
        request.setAttribute("key_userList", maps);
        request.getRequestDispatcher("/employeemanage.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }
}
