package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.AttendanceDao;
import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

@WebServlet("/delAttendanceServlet")
public class DelAttendanceServlet extends HttpServlet {
    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");


        int attendanceId = Integer.parseInt(request.getParameter("attendanceId"));
        System.out.println("刪除编号" + attendanceId);

        PrintWriter out = response.getWriter();
        AttendanceDao attendanceDAO = new AttendanceDaoImpl();

        JSONArray jsonArray = new JSONArray();
        List<String> str = new ArrayList<>();

        boolean b = attendanceDAO.deleteAttendance(attendanceId);

        if (b) {
            str.add("删除成功");
        } else {
            str.add("删除失败");
        }
        String jsonStr = JSON.toJSONString(str);
        jsonArray = JSONArray.parseArray(jsonStr);
        System.out.println(jsonArray);
        out.println(jsonArray);

    }
}

