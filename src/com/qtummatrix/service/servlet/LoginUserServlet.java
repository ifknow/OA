package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/loginUserServlet")
public class LoginUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        //登录验证
        String phone = (request.getParameter("phone"));
        String password = request.getParameter("password");

        //判断手机号是否为空
        if (com.alibaba.druid.util.StringUtils.isEmpty(phone)) {
            request.setAttribute("msg", "手机号不能为空！");
            request.setAttribute("password", password);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        if (com.alibaba.druid.util.StringUtils.isEmpty(password)) {
            request.setAttribute("msg", "密码不能为空");
            request.setAttribute("phone", phone);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        //获取输入框的验证码
        String verifycode = request.getParameter("verifycode");
        if (verifycode.equals("")) {
            System.out.println("验证码为空");
            request.setAttribute("msg", "验证码不能为空！");
            request.setAttribute("phone", phone);
            request.setAttribute("password", password);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }
        //验证码比对
        HttpSession session = request.getSession(true);
        String checkcode_server = (String) session.getAttribute("CHECKCODE_SERVER");
        session.removeAttribute("CHECKCODE_SERVER");//确保验证码一次性
        if (!checkcode_server.equalsIgnoreCase(verifycode)) {
            //验证码不正确
            //提示信息
            request.setAttribute("msg", "验证码错误！");
            //跳转登录页面
            request.getRequestDispatcher("/login.jsp").forward(request, response);
            return;
        }
        //登录验证手机号和密码是否正确
        try {
            User user = null;
            UserDao userDao = new UserDaoImpl();

            user = userDao.validateLogin(Long.parseLong(phone), password);
            if (user != null) {
                session.setAttribute("user", user);
                HttpSession session1 = request.getSession(true);
                response.sendRedirect("index.jsp");
            } else {
                request.setAttribute("msg", "用户名或密码错误！");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        doPost(request, response);
    }
}
