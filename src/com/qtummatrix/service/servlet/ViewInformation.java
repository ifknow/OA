package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;

@WebServlet("/viewInformation")
public class ViewInformation extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        int id = Integer.parseInt(request.getParameter("id"));
        //获取当前登录的用户
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        //获取当前登录的用户Id
        int operationUserId = user.getId();

        UserDao userDao = new UserDaoImpl();
        LinkedList<Map> maps = userDao.showUserInfo(operationUserId, id);

        request.setAttribute("usermaps", maps);
        request.getRequestDispatcher("/viewInformation.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }
}
