package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

@WebServlet("/fileUploadServlet")
public class FileUploadServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");

        //头像上传
        String uploadPath = "D:/Users/lenovo/program_02/refinesystem/web/img";
        //1. 文件上传的工厂
        DiskFileItemFactory factory = new DiskFileItemFactory();

        //2. 文件上传的核心类
        ServletFileUpload upload = new ServletFileUpload(factory);

        //3. 解析request对象
        int i = 1;
        int userId = 0;
        try {
            List<FileItem> fileItems = upload.parseRequest(request);
            for (FileItem fileItem : fileItems) {
                String name = fileItem.getName();//文件名称
                String fieldName = fileItem.getFieldName();//表单属性
                if (i == 1) {
                    userId = Integer.parseInt(fieldName);
                    i++;
                } else {
                    //文件属性上传
                    //4. 得到上传文件的输入流
                    InputStream ips = fileItem.getInputStream();
                    //先创建一个文件，将图片的名称进行用时间命名
                    String fineName = getFineName();
                    File file = new File(uploadPath + File.separator + fineName + name);
                    System.out.println(file);
                    FileOutputStream ops = new FileOutputStream(file);

                    //文件copy
                    IOUtils.copy(ips, ops);

                    //文件上传后将该员工id和头像名称修改到数据库中
                    UserDao userDao = new UserDaoImpl();
                    userDao.updateUserHeadimg(userId, "img/" + fineName + name);
                    //流的关闭
                    IOUtils.closeQuietly(ips);
                    IOUtils.closeQuietly(ops);
                }
            }

        } catch (FileUploadException e) {
            e.printStackTrace();
        }
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int operationUserId = user.getId();
        UserDao userDao = new UserDaoImpl();
        LinkedList<Map> maps = userDao.showUserInfo(operationUserId, userId);

        /*request.setAttribute("usermapss", maps);*/
        request.getRequestDispatcher("/index.jsp").forward(request, response);
//        response.sendRedirect("");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }

    public String getFineName() {
        Random random = new Random();
        String format = new SimpleDateFormat("yyyy_MM_dd_SSS").format(new Date());
        int a = random.nextInt(10);
        int b = random.nextInt(10);
        return format + a + b + "_";
    }
}
