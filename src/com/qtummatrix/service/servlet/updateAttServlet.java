package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.AttendanceDao;
import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/updateAtt")
public class updateAttServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        PrintWriter out = response.getWriter();

        int attendanceId = Integer.parseInt(request.getParameter("attendanceId"));
        //String username = req.getParameter("username");
        //String departmentName = req.getParameter("departmentName");
        String leaveType2 = request.getParameter("leaveType2");
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        String aduitState =request.getParameter("aduitState");
        System.out.println("attendanceId---"+attendanceId);
        System.out.println("leaveType2---"+leaveType2);
        System.out.println("startTime---"+startTime);
        System.out.println("endTime---"+endTime);
        System.out.println("aduitState---"+aduitState);


        AttendanceDao attendanceDao = new AttendanceDaoImpl();
        boolean b =attendanceDao.alterAttendance(attendanceId, leaveType2, startTime, endTime, aduitState);

        JSONArray jsonArray = new JSONArray();
        List<String> str = new ArrayList<>();

        if(b) {
            str.add("修改成功");
        }else {
            str.add("修改失败");
        }
        String jsonStr = JSON.toJSONString(str);
        jsonArray = JSONArray.parseArray(jsonStr);
        System.out.println(jsonArray);
        out.println(jsonArray);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
