package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author jsl
 * @date 2019/10/30 - 15:04
 */
@WebServlet("/UpdatecServlet")
public class UpdatecServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String position = request.getParameter("username");
        String changeType = request.getParameter("changeType");
        String newPosition = request.getParameter("newPosition");
        String newType = request.getParameter("newType");
        Map<String,String> map=new HashMap<String,String>();
        JSONObject resultObject =new JSONObject();
        UserDaoImpl userDao = new UserDaoImpl();
        userDao.updateChangeUser(position, changeType, newPosition, newType);
        PrintWriter out = response.getWriter();

        map.put("message","修改成功");
        String resultStr= JSON.toJSONString(map);
        resultObject=JSONObject.parseObject(resultStr);
        out.println(resultObject);
        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
