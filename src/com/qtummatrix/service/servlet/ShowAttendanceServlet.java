package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.AttendanceDao;

import com.qtummatrix.dao.daoImpl.AttendanceDaoImpl;
import com.qtummatrix.entity.AttendanceBO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@WebServlet("/showAttendanceServlet")
public class ShowAttendanceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");

        System.out.println(request.getParameter("leaveclass"));
        String leaveclass = request.getParameter("leaveclass");

        PrintWriter  out = response.getWriter();



        //创建一个总的list
        List<Object> allList = new ArrayList<>();
        List<AttendanceBO> aboList = new ArrayList<>();
        List<AttendanceBO> aboListCount = new ArrayList<>();
        List<AttendanceBO> aboListCountDocu = new ArrayList<>();

        AttendanceDao attendanceDao = new AttendanceDaoImpl();
//        LinkedList<Map> attendanceList = attendanceDao.showAttendanceInfo(leaveclass);

        JSONArray jsonArray = new JSONArray();

        int count = 0;
        //分类查询考勤用户
        aboListCount = attendanceDao.searchAllUserLeave(leaveclass);
        //获取用户列表
        aboList = attendanceDao.searchAllAttendances(leaveclass);
        //获取总人数
        count = attendanceDao.countLeaveNumber(leaveclass);
        //获取部门总人数
        aboListCountDocu = attendanceDao.countLeaveNumberDocu(leaveclass);
        System.out.println("获取总人数"+count);
        System.out.println("部门总人数"+aboListCountDocu.toString());
        allList.add(aboList);
        allList.add(aboListCount);
        allList.add(count);
        allList.add(aboListCountDocu);

        System.out.println(allList.toString());
        if(!allList.isEmpty()) {
            String jsonStr = JSON.toJSONString(allList);
            jsonArray = JSONArray.parseArray(jsonStr);
            System.out.println(jsonArray);
            out.println(jsonArray);
        }
    }



    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request,response);
    }
}
