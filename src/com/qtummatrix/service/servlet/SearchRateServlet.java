package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.Pie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/SearchRateServlet")
public class SearchRateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        UserDaoImpl userDao = new UserDaoImpl();
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String time = request.getParameter("time");
        String starttime = request.getParameter("startTime");
        String endtime = request.getParameter("endTime");
        PrintWriter out = response.getWriter();
        System.out.println(starttime + "```" + endtime);
        Pie pie1 = userDao.attendanceRate(time, starttime, endtime);
        Pie pie2 = userDao.vacateRate(time, starttime, endtime);
        Pie pie3 = userDao.travelRate(time, starttime, endtime);
        Pie pie4 = userDao.lateRate(time, starttime, endtime);
        List<Pie> list = new ArrayList<>();
        list.add(pie1);
        list.add(pie2);
        list.add(pie3);
        list.add(pie4);
        /*for (Pie pie : list) {
            System.out.println(pie.toString());
        }*/
        JSONArray jsonArray = new JSONArray();
        String jasonString = JSON.toJSONString(list); //把list对象转换为json字符串
        jsonArray = JSONArray.parseArray(jasonString);//把json字符串转换为json数组
        out.println(jsonArray);
        out.close();
    }
}
