package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/ExamineInfoServlet")
public class ExamineInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        UserDaoImpl userDao = new UserDaoImpl();
        PrintWriter out = response.getWriter();
        List<Integer> list = new ArrayList<>();
        list = userDao.examine();
        JSONArray jsonArray = new JSONArray();
        String jasonString = JSON.toJSONString(list); //把list对象转换为json字符串
        jsonArray = JSONArray.parseArray(jasonString);//把json字符串转换为json数组
        out.println(jsonArray);
        out.close();
    }
}
