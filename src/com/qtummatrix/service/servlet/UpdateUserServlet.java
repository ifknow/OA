package com.qtummatrix.service.servlet;

import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

@WebServlet("/updateUserServlet")
public class UpdateUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");

        //声明User对象,存储前端传过来的员工数据
        User user = new User();
        UserDao userDao = new UserDaoImpl();
        HttpSession session = request.getSession();
        User user1 = (User) session.getAttribute("user");
        int operationUserId = user1.getId();

        //**验证输入是否为空将会在最后用前端验证
        String id = request.getParameter("id");
        String headimg = request.getParameter("headimg");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        long tel = Long.parseLong(request.getParameter("tel"));
        String email = request.getParameter("email");
        long qq = Long.parseLong(request.getParameter("qq"));
        long uid = Long.parseLong(request.getParameter("uid"));
        String address = request.getParameter("address");
        String department = request.getParameter("department");
        String position = request.getParameter("position");
        String entrytime = request.getParameter("entrytime");

        user.setId(Integer.parseInt(id));
        user.setHeadimg(headimg);
        user.setUsername(name);
        user.setSex(sex);
        user.setPhone(tel);
        user.setEmail(email);
        user.setQq(qq);
        user.setUID(uid);
        user.setAddress(address);
        user.setDepartmentName(department);
        user.setPositionName(position);
        user.setEntryTime(entrytime);

        LinkedList<User> linkedList = userDao.updateUser(operationUserId, user);

        /*System.out.println("UpdateUserServlet 测试数据");
        for (User user2 : linkedList) {
            System.out.println(user2);
        }*/

        request.setAttribute("usermapss", linkedList);
        request.getRequestDispatcher("/eidtUser.jsp").forward(request, response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }
}
