package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.qtummatrix.dao.UserDao;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;

@WebServlet("/delUserServlet")
public class DelUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        //测试
        String uid = request.getParameter("id");
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        int operationUserId = user.getId();
        if (!request.getParameter("id").equals("")) {
            int id = Integer.parseInt(request.getParameter("id"));
            UserDao userDao = new UserDaoImpl();
            int i = userDao.delUser(operationUserId, id);
            // 删除成功
            if (i > 0) {
                HashMap<String, Object> map = new HashMap<String, Object>();
                map.put("tt", true);
                String json = JSON.toJSONString(map);
                response.getWriter().write(json);
            } else {
                // 删除失败
                response.sendRedirect("index.jsp");

            }

        } else {
            //没有成功传值
            response.sendRedirect("index.jsp");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        this.doPost(request, response);
    }
}
