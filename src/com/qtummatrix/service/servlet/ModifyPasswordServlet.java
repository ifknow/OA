package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/ModifyPasswordServlet")
public class ModifyPasswordServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out=response.getWriter();
        Map<String,String> map=new HashMap<String,String>();
        JSONObject resultObject =new JSONObject();
        HttpSession session=request.getSession();
        Object object=session.getAttribute("user");
        User user=(User)object;
        int id=user.getId();
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");
        UserDaoImpl userDao=new UserDaoImpl();
        boolean b=userDao.changePassword(id,oldPassword,newPassword);
        if (b){
           map.put("message","修改成功！");

        }
        else {
           map.put("message","密码错误,修改失败！");
        }
        String resultStr= JSON.toJSONString(map);
        resultObject=JSONObject.parseObject(resultStr);

        out.println(resultObject);
        out.close();

    }
}
