package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.RecruitInfo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/RecruitInfoServlet")
public class RecruitInfoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        UserDaoImpl userDao = new UserDaoImpl();
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter out = response.getWriter();
        String type1 = request.getParameter("type1");
        String type2 = request.getParameter("type2");
        String type3 = request.getParameter("type3");
        String type4 = request.getParameter("type4");
        RecruitInfo recruitInfo1 = userDao.hiringSituation(type1);
        RecruitInfo recruitInfo2 = userDao.hiringSituation(type2);
        RecruitInfo recruitInfo3 = userDao.hiringSituation(type3);
        RecruitInfo recruitInfo4 = userDao.hiringSituation(type4);
        List<RecruitInfo> list = new ArrayList<>();
        list.add(recruitInfo1);
        list.add(recruitInfo2);
        list.add(recruitInfo3);
        list.add(recruitInfo4);
        JSONArray jsonArray = new JSONArray();
        String jasonString = JSON.toJSONString(list); //把list对象转换为json字符串
        jsonArray = JSONArray.parseArray(jasonString);//把json字符串转换为json数组
        out.println(jsonArray);
        out.close();
    }
}
