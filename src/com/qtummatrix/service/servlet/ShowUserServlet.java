package com.qtummatrix.service.servlet;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.mysql.cj.util.StringUtils;
import com.qtummatrix.dao.daoImpl.UserDaoImpl;
import com.qtummatrix.entity.ChangeUser;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author jsl
 * @date 2019/10/29 - 16:34
 */
@WebServlet("/ShowUserServlet")
public class ShowUserServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String username = request.getParameter("text");
        String select = request.getParameter("option");
      /*  String start = request.getParameter("start");
        String end = request.getParameter("end");*/
        String n = request.getParameter("n");
        UserDaoImpl userDao = new UserDaoImpl();

        List<ChangeUser> list = new ArrayList<>();
        if (StringUtils.isNullOrEmpty(username) && StringUtils.isNullOrEmpty(select)) {
/*
        if(StringUtils.isNullOrEmpty(username) && StringUtils.isNullOrEmpty(select) &&
                StringUtils.isNullOrEmpty(start) && StringUtils.isNullOrEmpty(end)){
*/
            list = userDao.ChangeUser(Integer.parseInt(n));
        } else {
            list = userDao.showUser(username, select);
/*
            list = userDao.showUser(username,select,start,end);
*/
        }
        JSONArray jsonArray = new JSONArray();
        String jsonString = JSON.toJSONString(list);
        jsonArray = JSONArray.parseArray(jsonString);
        PrintWriter out = response.getWriter();
        out.println(jsonArray);
        out.close();


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException,
            IOException {
        doPost(request, response);
    }
}
