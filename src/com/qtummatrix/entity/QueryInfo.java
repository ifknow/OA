package com.qtummatrix.entity;

/**
 * @Author Gongshiyong
 * @Date 2019-10-24 10:22
 */
public class QueryInfo {

    private int currentPage = 1; //用户当前看的页码
    private int pagesize = 5;  //页面记录数
    private int startindex; //记住用户看的页的在数据库的起始位置

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public int getStartindex() {
        this.startindex = (this.currentPage - 1) * this.pagesize;
        return startindex;
    }
}
