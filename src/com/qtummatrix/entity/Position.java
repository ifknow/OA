package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 16:18
 */
public class Position {
    private int id;             //职位ID
    private int departmentId;   //部门ID
    private int positionName;   //职位名称
    private int planNumber;     //计划招聘人数

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getPositionName() {
        return positionName;
    }

    public void setPositionName(int positionName) {
        this.positionName = positionName;
    }

    public int getPlanNumber() {
        return planNumber;
    }

    public void setPlanNumber(int planNumber) {
        this.planNumber = planNumber;
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", departmentId=" + departmentId +
                ", positionName=" + positionName +
                ", planNumber=" + planNumber +
                '}';
    }
}
