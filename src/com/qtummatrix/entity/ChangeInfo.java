package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 16:03
 */
/**
 * 职位变动
 */
public class ChangeInfo {
    private String username;                //标识ID
    private String headimg;             //员工ID
    private String info;
    private String changetime;      //变动时间
    public void setUsername(String username) {
        this.username = username;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setChangetime(String changetime) {
        this.changetime = changetime;
    }

    public String getUsername() {
        return username;
    }

    public String getHeadimg() {
        return headimg;
    }

    public String getInfo() {
        return info;
    }

    public String getChangetime() {
        return changetime;
    }

    @Override
    public String toString() {
        return "ChangeInfo{" +
                "username='" + username + '\'' +
                ", headimg='" + headimg + '\'' +
                ", info='" + info + '\'' +
                ", changetime='" + changetime + '\'' +
                '}';
    }
}
