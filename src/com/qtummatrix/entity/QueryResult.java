package com.qtummatrix.entity;

import java.util.List;

/**
 * @Author Gongshiyong
 * @Date 2019-10-24 10:27
 */
public class QueryResult {

    private List list;      //记住用户看的页的数据
    private int totalrecord;//记住总记录数

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public int getTotalrecord() {
        return totalrecord;
    }

    public void setTotalrecord(int totalrecord) {
        this.totalrecord = totalrecord;
    }
}
