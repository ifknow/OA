package com.qtummatrix.entity;

/**
 * @Author Gongshiyong
 * @Date 2019-10-22 10:47
 */

import java.util.Date;

/**
 * 操作记录表
 */
public class Operation {

    private String logintime;
    private String headimg;
    private String operationinfo1;
    private String operationinfo2="";

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public String getOperationinfo1() {
        return operationinfo1;
    }

    public void setOperationinfo1(String operationinfo1) {
        this.operationinfo1 = operationinfo1;
    }

    public String getOperationinfo2() {
        return operationinfo2;
    }

    public void setOperationinfo2(String operationinfo2) {
        this.operationinfo2 = operationinfo2;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "logintime='" + logintime + '\'' +
                ", headimg='" + headimg + '\'' +
                ", operationinfo1='" + operationinfo1 + '\'' +
                ", operationinfo2='" + operationinfo2 + '\'' +
                '}';
    }
}
