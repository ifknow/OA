package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/25 - 14:55
 */
public class Pie {
    //出勤率   请假率  迟到率  出差率
    private String rateType;
    private int value1;
    //总人数
    private int value2;

    public String getRateType() {
        return rateType;
    }

    public void setRateType(String rateType) {
        this.rateType = rateType;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    @Override
    public String toString() {
        return "Pie{" +
                "rateType='" + rateType + '\'' +
                ", value1=" + value1 +
                ", value2=" + value2 +
                '}';
    }
}
