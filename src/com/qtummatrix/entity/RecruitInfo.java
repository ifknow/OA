package com.qtummatrix.entity;


public class RecruitInfo {
    private String departmentName;
    private int value1;
    private int value2;

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public int getValue1() {
        return value1;
    }

    public void setValue1(int value1) {
        this.value1 = value1;
    }

    public int getValue2() {
        return value2;
    }

    public void setValue2(int value2) {
        this.value2 = value2;
    }

    @Override
    public String toString() {
        return "RecruitInfo{" +
                "departmentName='" + departmentName + '\'' +
                ", value1=" + value1 +
                ", value2=" + value2 +
                '}';
    }
}
