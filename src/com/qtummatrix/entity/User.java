package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 16:09
 */

import java.util.Date;

/**
 * 员工信息
 */
public class User {
    private int id;                 //员工ID
    private String password;        //密码
    private String sex;             //性别 1.男 2.女
    private String username;        //用户姓名
    private long phone;             //电话号码
    private long qq;                 //QQ号
    private long UID;               //身份证号
    private String email;           //邮箱
    private String headimg;         //头像
    private String address;         //地址
    private int departmentId;       //部门ID
    private int positionId;      //职位ID 1.主管 2.设计师 3.经理 4.设计总监
    private String entryTime;       //入职时间
    private String mark;            //备注
    private String IsAdmin;
    private String departmentName;
    private String positionName;

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getIsAdmin() {
        return IsAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        IsAdmin = isAdmin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public long getQq() {
        return qq;
    }

    public void setQq(long qq) {
        this.qq = qq;
    }

    public long getUID() {
        return UID;
    }

    public void setUID(long UID) {
        this.UID = UID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    public String getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(String entryTime) {
        this.entryTime = entryTime;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", password='" + password + '\'' +
                ", sex='" + sex + '\'' +
                ", username='" + username + '\'' +
                ", phone=" + phone +
                ", qq=" + qq +
                ", UID=" + UID +
                ", email='" + email + '\'' +
                ", headimg='" + headimg + '\'' +
                ", address='" + address + '\'' +
                ", departmentId=" + departmentId +
                ", positionId='" + positionId + '\'' +
                ", entryTime='" + entryTime + '\'' +
                ", mark='" + mark + '\'' +
                '}';
    }
}
