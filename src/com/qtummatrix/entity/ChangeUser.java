package com.qtummatrix.entity;

import java.util.Date;

/**
 * @author jsl
 * @date 2019/10/29 - 10:12
 */
public class ChangeUser {
    private String headimg;
    private String username;
    private String sex;
    private String dempartmentName;
    private String positionName;
    private String changetime;

    public String getChangetime() {
        return changetime;
    }

    public void setChangetime(String changetime) {
        this.changetime = changetime;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getDempartmentName() {
        return dempartmentName;
    }

    public void setDempartmentName(String dempartmentName) {
        this.dempartmentName = dempartmentName;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    @Override
    public String toString() {
        return "ChangeUser{" +
                "headimg='" + headimg + '\'' +
                ", username='" + username + '\'' +
                ", sex='" + sex + '\'' +
                ", dempartmentName='" + dempartmentName + '\'' +
                ", positionName='" + positionName + '\'' +
                ", changetime=" + changetime +
                '}';
    }
}
