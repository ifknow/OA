package com.qtummatrix.entity;

import java.io.Serializable;

public class AttendanceBO implements Serializable {
    private static final long serialVersionUID = 7495460185204337627L;
    private int attendanceId;
    private int userId;
    private String username;
    private String departmentName;
    private String leaveType2;
    private String leaveType1;
    private String aduitState;
    private String startTime;
    private String endTime;
    private String headimg;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getAttendanceId() {
        return attendanceId;
    }

    public void setAttendanceId(int attendanceId) {
        this.attendanceId = attendanceId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getLeaveType2() {
        return leaveType2;
    }

    public void setLeaveType2(String leaveType2) {
        this.leaveType2 = leaveType2;
    }

    public String getLeaveType1() {
        return leaveType1;
    }

    public void setLeaveType1(String leaveType1) {
        this.leaveType1 = leaveType1;
    }

    public String getAduitState() {
        return aduitState;
    }

    public void setAduitState(String aduitState) {
        this.aduitState = aduitState;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getHeadimg() {
        return headimg;
    }

    public void setHeadimg(String headimg) {
        this.headimg = headimg;
    }

    public AttendanceBO(int attendanceId, int userId, String username, String departmentName, String leaveType2,
                        String leaveType1, String aduitState, String startTime, String endTime, String headimg,
                        int count) {
        super();
        this.attendanceId = attendanceId;
        this.userId = userId;
        this.username = username;
        this.departmentName = departmentName;
        this.leaveType2 = leaveType2;
        this.leaveType1 = leaveType1;
        this.aduitState = aduitState;
        this.startTime = startTime;
        this.endTime = endTime;
        this.headimg = headimg;
        this.count = count;
    }

    public AttendanceBO() {
        super();
    }

    @Override
    public String toString() {
        return "AttendanceBO [attendanceId=" + attendanceId + ", userId=" + userId + ", username=" + username
                + ", departmentName=" + departmentName + ", leaveType2=" + leaveType2 + ", leaveType1=" + leaveType1
                + ", aduitState=" + aduitState + ", startTime=" + startTime + ", endTime=" + endTime + ", headimg="
                + headimg + "]";
    }


}

