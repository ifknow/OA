package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 16:16
 */

/**
 * 部门类
 */
public class Dempartment {
    private int id;                 //部门ID
    private int dempartmentName;    //部门名称

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDempartmentName() {
        return dempartmentName;
    }

    public void setDempartmentName(int dempartmentName) {
        this.dempartmentName = dempartmentName;
    }

    @Override
    public String toString() {
        return "Dempartment{" +
                "id=" + id +
                ", dempartmentName=" + dempartmentName +
                '}';
    }
}
