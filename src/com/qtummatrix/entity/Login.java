package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 15:52
 */

/**
 * 登录信息类
 */
public class Login {
    private int id;                 //登陆ID
    private int userid;             //用户ID
    private String loginname;       //登陆名称
    private String password;        //密码
    private String logintime;       //登陆时间

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getLoginname() {
        return loginname;
    }

    public void setLoginname(String loginname) {
        this.loginname = loginname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    @Override
    public String toString() {
        return "login{" +
                "id=" + id +
                ", userid=" + userid +
                ", loginname='" + loginname + '\'' +
                ", password='" + password + '\'' +
                ", logintime='" + logintime + '\'' +
                '}';
    }
}
