package com.qtummatrix.entity;

/**
 * @author 贾双龙
 * @date 2019/10/21 - 15:45
 */

import java.sql.Date;

/**
 * 考勤类
 */
public class Attendance {
    private int id;                 //考勤ID
    private int userid;             //员工ID
    private String leaveclass;       //考勤分类 1.请假 2.迟到/早退 3.出差 4.加班 5.旷工
    private String leavetype;      //考勤类型 如1.调休 2.病假 3.产假 4.事假
    private String  aduitState;      //审核状态 1.未审核 2.已审核
    private Date starttime;       //开始日期
    private Date endtime;         //结束日期

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getLeaveclass() {
        return leaveclass;
    }

    public void setLeaveclass(String leaveclass) {
        this.leaveclass = leaveclass;
    }

    public String getLeavetype() {
        return leavetype;
    }

    public void setLeavetype(String leavetype) {
        this.leavetype = leavetype;
    }

    public String getAduitState() {
        return aduitState;
    }

    public void setAduitState(String  aduitState) {
        this.aduitState = aduitState;
    }

    public Date getStarttime() {
        return starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    @Override
    public String toString() {
        return "attendance{" +
                "id=" + id +
                ", userid=" + userid +
                ", leaveclass='" + leaveclass + '\'' +
                ", leavetype='" + leavetype + '\'' +
                ", aduitState='" + aduitState + '\'' +
                ", starttime='" + starttime + '\'' +
                ", endtime='" + endtime + '\'' +
                '}';
    }
}
