package com.qtummatrix.dao;

import com.qtummatrix.entity.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @date 2019/10/21 - 20:41
 */
public interface UserDao {

    public List<ChangeUser> ChangeUser(int n);

    public List<ChangeUser> showUser(String username, String changeInfo);

    public void updateChangeUser(String username, String changeType, String newPosition, String newType);

    /**
     * 验证登陆
     *
     * @param phone    手机号
     * @param password 用户密码
     * @return
     */
    public User validateLogin(long phone, String password);

    /**
     * 修改密码
     *
     * @param oldPassword 原密码
     * @param newPassword 新密码
     * @return
     */
    public boolean changePassword(int id, String oldPassword, String newPassword);

    /**
     * 最近入职员工信息
     *
     * @return
     */
    public List<User> recentInfo();

    /**
     * 最近人事变动信息
     *
     * @return
     */
    public List<ChangeInfo> recentChangeInfo();

    /**
     * 条件查询员工
     *
     * @param operationId 操作的用户iD
     * @param username    用户名
     * @param department  部门名
     * @param starttime   入职时间
     * @param endtime     结束时间
     * @return
     */
    public LinkedList<Map> search(int operationId, String username, String department, String starttime,
                                  String endtime);

    /**
     * 出勤率
     *
     * @return
     */
    public Pie attendanceRate(String time, String starttime, String endtime);

    /**
     * 请假率
     *
     * @param time
     * @return
     */
    public Pie vacateRate(String time, String starttime, String endtime);

    /**
     * 出差率
     *
     * @param time
     * @return
     */
    public Pie travelRate(String time, String starttime, String endtime);

    /**
     * 迟到率
     *
     * @param time
     * @return
     */
    public Pie lateRate(String time, String starttime, String endtime);

    /**
     * 部门类型招人情况
     *
     * @return
     */
    public RecruitInfo hiringSituation(String departmentType);


    public List<Operation> operationInfo();

    //员工信息管理模块

    /**
     * 查询所有员工信息显示在员工信息管理中
     *
     * @return
     */
    public LinkedList<Map> selectUserInfo(int operationId, int x);

    /**
     * 添加用户信息
     *
     * @param operationId 操作的员工Id
     * @param user        要添加的员工信息
     */
    public int addUser(int operationId, User user);

    /**
     * 删除员工信息
     *
     * @param operationId 操作数据的用户Id
     * @param id          被操作的用户ID
     */
    public int delUser(int operationId, int id);

    /**
     * 根据部门查询员工
     *
     * @param department
     * @return
     */
    public List<User> showUserByDepartment(String department);

    /**
     * 根据职称查询员工
     */
    public List<User> showUserByWork(String workName);

    /**
     * 根据ID查询员工详细信息
     */
    public LinkedList<Map> showUserInfo(int opetationId, int id);

    /**
     * 搜索条件查询
     *
     * @param username       用户名
     * @param departmentName 部门
     * @param beginTime      开始时间
     * @param overTime       结束时间
     * @return
     */
    public List<User> showUserBySearch(String username, String departmentName, String beginTime, String overTime);

    /**
     * 修改员工信息
     *
     * @param user
     */
    public LinkedList<User> updateUser(int operationId, User user);

    public User updateUser1(int operationId, User user);

    /**
     * 考勤
     */

    /**
     * 审核 添加员工考勤信息
     */
    public void addCheck(long phone, String checkType);

    /**
     * 编辑员工考勤信息
     */
    public void updateCheck();

    /**
     * 查询总人数
     */
    public int selectTotalPerson();

    /**
     * 获取下一页
     *
     * @param page
     * @return
     */
    public LinkedList<Map> getNextPage(int opetationId, int page);

    /**
     * 获取上一页
     *
     * @param page
     * @return
     */
    public LinkedList<Map> getPrevious(int operationId, int page);

    public void updateUserHeadimg(int id, String headimgURL);


}

