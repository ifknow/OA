package com.qtummatrix.dao;

import com.qtummatrix.entity.Attendance;
import com.qtummatrix.entity.AttendanceBO;

import java.sql.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public interface AttendanceDao {
    //员工考勤管理模块

    /**
     *  显示 各考勤分类的 员工考勤信息
     * @param leaveclass 考勤分类
     * @return
     */
    public LinkedList<Map> showAttendanceInfo(String leaveclass);

    /**
     * 添加考勤  向考勤表中添加信息
     * @param attendance
     */
    public int addAttendance(Attendance attendance);

    /**
     * 通过用户姓名 查找用户id
     * @param username  用户姓名
     * @return
     */
    public int search(String username);


    /**
     * 编辑考勤信息
     * @param attendance
     * @return
     */
    public int updateCheck(Attendance attendance);


    /**
     * 删除考勤
     * @param id  考勤id
     * @return
     */
    public int delAttendance(int id);



    /**
     *  查询所有考勤信息
     * @param leaveclass  考勤分类
     * @return
     */
    public List<AttendanceBO> searchAllAttendances(String leaveclass);


    /**
     * 分类查询考勤总人数
     * @param leaveclass
     * @return
     */
    public List<AttendanceBO> searchAllUserLeave(String leaveclass);



    /**
     *  统计请假的总人数（所有部门加一起）
     * @param leaveclass 考勤分类
     * @return
     */
    public Integer countLeaveNumber(String leaveclass);




    /**
     *  统计各部门（某个部门）请假、旷工、或迟到早退人数（求所占的比例）
     * @param leaveclass 考勤分类
     * @return
     */
    public List<AttendanceBO> countLeaveNumberDocu(String leaveclass);


    /**
     * 检查用户名是否已存在（已存在返回true；不存在返回false）
     * @param username
     * @return Boolean
     */
    public Boolean checkUsername(String username);




    /**
     * 添加员工考勤信息
     * @param userId        员工ID
     * @param leaveclass    考勤分类
     * @param leavetype     考勤类型
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @param aduitState    审核状态
     * @return
     */
    public Boolean addAttenDance(int userId, String leaveclass,String leavetype,String startTime ,String endTime,String aduitState);



    /**
     * 根据员工考勤Id查询（精确查询）
     * @param attendanceId
     * @return
     */
    public AttendanceBO queryAttendance(int attendanceId);



    /**
     * 编辑员工考勤信息
     * @param attendanceId
     * @param leaveType2
     * @param startTime
     * @param endTime
     * @param aduitState
     * @return
     */
    public Boolean alterAttendance(int attendanceId, String leaveType2,String startTime,String endTime,String aduitState);


    /**
     * 删除员工考勤信息
     * @param attendanceId
     * @return
     */
    public Boolean deleteAttendance(int attendanceId);




    /**
     * 条件查询
     * @param username
     * @param startTime
     * @param endTime
     * @param aduitState
     * @return
     */
    public List<AttendanceBO> queryAttendances(String username, Date startTime, Date endTime, String aduitState);



}
