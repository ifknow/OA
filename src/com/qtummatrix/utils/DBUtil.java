package com.qtummatrix.utils;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;
import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;

public class DBUtil {

	private static Properties properties = new Properties();

	static {
		try {
			properties.load(DBUtil.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 创建连接对象
	 */
	public static Connection getConnection() throws Exception {
		DataSource ds = DruidDataSourceFactory.createDataSource(properties);
		Connection connection = ds.getConnection();
		return connection;
	}

	public static void close(Connection connection, Statement statement, ResultSet resultSet) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 时间类型转字符串
	 *
	 * @param date
	 * @return
	 */
	public static String dateToString(Date date) {
		return date.toString();
	}

	/**
	 * 字符串转时间类型
	 *
	 * @param str
	 * @return
	 */
	public static Date StringtoDate(String str) {
		return Date.valueOf(str);
	}
}
