<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>添加用户</title>
    <!-- 指定字符集 -->
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport视口：网页可以根据设置的宽度自动进行适配，在浏览器的内部虚拟一个容器，容器的宽度与设备的宽度相同。
    width: 默认宽度与设备的宽度相同
    initial-scale: 初始的缩放比，为1:1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>添加用户</title>
    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <%--    <script src="js/jquery-2.1.0.min.js"></script>--%>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.min.js"></script>
    <script>
        function addUser() {
            var sSex = $("#sSex option:selected").val();
            var sDepartment = $("#sDepartment option:selected").val();
            var sPosition = $("#sPosition option:selected").val();
            var sPermission = $("#sPermission option:selected").val();
            console.log(sSex + "  " + sDepartment + "  " + sPosition + "  " + sPermission);
            $.ajax({
                url: "addUserServlet",
                type: "post",
                async: true,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    sHeadimg: $("#sHeadimg").val(),
                    sName: $("#sName").val(),
                    sSex: sSex,
                    sPass: $("#sPass").val(),
                    sTel: $("#sTel").val(),
                    sEmial: $("#sEmial").val(),
                    sQQ: $("#sTel").val(),
                    sUId: $("#sUId").val(),
                    sAddress: $("#sAddress").val(),
                    sDepartment: sDepartment,
                    sPosition: sPosition,
                    sPermission: sPermission,
                },
                //请求成功后，回调函数
                success: function (data) {
                    alert("用户添加成功！");
                    loadPage("addUser.jsp");
                }
            })
        }
    </script>
</head>
<body>
<div class="container">
    <center><h3>添加员工</h3></center>
    <%--    <form action="${pageContext.request.contextPath}/addUserServlet" method="post" style="width: 400px;margin: auto;">--%>
    <form style="width: 400px;margin: auto;" id="form">
        <div class="form-group">
            <label for="sName">姓名：</label>
            <input type="text" class="form-control" id="sName" name="sName"
                   placeholder=" 请输入姓名">
            <span id="s_count"></span>
        </div>
        <div class="form-group">
            <label for="sPass">密码：</label>
            <input type="text" class="form-control" name="sPass" id="sPass"
                   placeholder="请输入密码"/>
        </div>
        <div>
            <label for="sSex">请选择性别：</label>
            <select class="form-control" name="sSex" id="sSex">
                <option>男</option>
                <option>女</option>
            </select>
        </div>
        <div class="form-group">
            <label for="sQQ">QQ：</label>
            <input type="text" class="form-control" name="sQQ" id="sQQ"
                   placeholder="请输入QQ"/>
        </div>
        <div class="form-group">
            <label for="sEmial">电子邮箱：</label>
            <input type="text" class="form-control" name="sEmial" id="sEmial"
                   placeholder="请输入电子邮箱"/>
        </div>
        <div class="form-group">
            <label for="sTel">手机号码：</label>
            <input type="text" class="form-control" name="sTel" id="sTel"
                   placeholder="请输入手机号码"/>
        </div>
        <div class="form-group">
            <label for="sUId">身份证号：</label>
            <input type="text" class="form-control" id="sUId"
                   placeholder="请输入身份证号"/>
        </div>
        <div class="form-group">
            <label for="sAddress">住址：</label>
            <input type="text" class="form-control" id="sAddress"
                   placeholder="请输入您的住址"/>
        </div>

        <div class="form-group">
            <label>请选择部门：1、设计部 2、美术部 3、市场部 4、研发部</label>
            <select class="form-control" name="sDepartment" id="sDepartment">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        </div>
        <div class="form-group">
            <label>请选择职位：1、经理 2、设计师 3、技术总监 4、主管</label>
            <select class="form-control" name="sPosition" id="sPosition">
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
            </select>
        </div>


        <div class="form-group">
            <label>设置权限：</label>
            <select class="form-control" name="sPermission" id="sPermission">
                <option>管理员</option>
                <option>普通用户</option>
            </select>
        </div>

        <div class="form-group">
            <label>状态：1、正常 2、离职</label>
            <select class="form-control" name="sMark" id="sMark">
                <option>1</option>
                <option>2</option>
            </select>
        </div>
        <div class="form-group" style="text-align: center">
            <input class="btn btn-primary" type="button" value="添加" onclick="javascript:addUser()"/>
            <input class="btn btn-danger" type="reset" value="重置"/>
            <input class="btn btn-default" type="button" value="返回" onclick="loadPage('employeemanage.jsp')"/>
        </div>
    </form>

</div>
</body>

</html>
