window.onload = PieChart();

function PieChart() {
    var time = $("#selectid").val();
    var startTime = $("#d4311").val();
    var endTime = $("#d4312").val();
    var startdate = stringToDate(startTime);
    var enddate = stringToDate(endTime);
    var now = new Date();
    if (now.getTime() < startdate.getTime()) {
        cleardate1()
        $("#date-message").html("所选日期超过最大日期限制！");
        return;
    }
    if (now.getTime() < enddate.getTime()) {
        cleardate2()
        $("#date-message").html("所选日期超过最大日期限制！");
        return;
    }

    if (startdate.getTime() < enddate.getTime()) {
        $("#date-message").html("结束日期必须在开始日期之后！");
        cleardate2()
        return;
    }
    $("#date-message").html("")
    $.ajax({

        url: "SearchRateServlet",
        type: "POST",
        async: true,
        catch: false,
        dataType: "json",//表示请求的返回数据格式{"username":"sss","password":"111"}
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data: {
            time: time,
            startTime: startTime,
            endTime: endTime
        },
        success: function (data) {//回调函数
            for (var i = 0; i < data.length; i++) {
                var myChart = echarts.init($(".pie").eq(i)[0]);
                var name = data[i].rateType;
                var value1 = data[i].value1;
                if (value1 == null) {
                    value1 = 0;
                }
                var value2 = data[i].value2;
                var value = (value1 / (value2 + value1)) * 100;
                value = parseFloat(value).toFixed(2) + "%";

                option = {
                    tooltip: {
                        trigger: 'item',
                        formatter: "{a} <br/>{b}: {c} ({d}%)"
                    },
                    title: {
                        text: value,
                        subtext: name,
                        x: 'center',
                        y: 'center',
                        itemGap: 25,
                        textStyle: {
                            fontSize: '16',
                            "color": "orange",
                            fontWeight: 'bold',
                        }, subtextStyle: {
                            fontFamily: 'Arial, Verdana, sans...',
                            fontSize: 12,
                            "color": "black",
                        },
                    },

                    series: [
                        {
                            name: '出勤信息',
                            type: 'pie',
                            radius: ['70%', '80%'],
                            avoidLabelOverlap: false,
                            label: {
                                normal: {

                                    show: false,
                                    position: 'center'
                                },
                                emphasis: {
                                    show: true,
                                    textStyle: {
                                        fontSize: '15',
                                        fontWeight: 'bold'
                                    }
                                }
                            },
                            labelLine: {
                                normal: {
                                    show: false
                                }
                            },
                            data: [
                                {value: value1},
                                {value: value2},
                            ]
                        }
                    ]
                };
                myChart.setOption(option);

            }
        },
        error: function () {
            alert("changeInfo失败")
        }
    })
}

function stringToDate(str) {
    str = str.replace(/-/g, "/");
    var date = new Date(str);
    return date;
}

function cleardate1() {
    $("#d4311").val("");

}

function cleardate2() {
    $("#d4312").val("");
}

function cleardate() {
    cleardate1();
    cleardate2();
    PieChart();
}