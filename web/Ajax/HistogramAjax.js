window.onload=histogram();

function histogram() {
    $.ajax({
        url:"RecruitInfoServlet",
        type:"POST",
        async:true,
        catch:false,
        dataType:"json",//表示请求的返回数据格式{"username":"sss","password":"111"}
        contentType:"application/x-www-form-urlencoded;charset=utf-8",
        data:{
            type1:'市场部',
            type2:'设计部',
            type3:'工程部',
            type4:'财务部'
        },
        success:function (data) {//回调函数
            for (var i=0;i<data.length;i++){
                var myChart= echarts.init($(".histogram").eq(i)[0]);
                var departmentName=data[i].departmentName;
                var value1=data[i].value1;
                var value2=data[i].value2;
                if (value2==null){
                    value1=0;
                }
                option = {
                    color: ['#3398DB'],
                    tooltip : {
                        trigger: 'axis',
                        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                        }
                    },
                    title:{
                        text:departmentName,
                        x:'center',
                        y: 'buttom',
                        itemGap: 25,
                        textStyle: {
                            fontSize: '16',
                            "color": "orange",
                            fontWeight: 'bold',
                        }
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis : [
                        {
                            type : 'category',
                            data : ['计划', '实际'],
                            axisTick: {
                                alignWithLabel: true
                            }
                        }
                    ],
                    yAxis : [
                        {
                            type : 'value'
                        }
                    ],
                    series : [
                        {
                            name:'招人情况',
                            type:'bar',
                            barWidth: '60%',
                            data:[value1,value2]
                        }
                    ]
                };

                myChart.setOption(option);

            }
        },
        error:function () {
            alert("changeInfo失败")
        }
    })
}