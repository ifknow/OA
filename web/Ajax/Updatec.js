var username = "";

function update(node) {
    var name = node.parentNode.parentNode.parentNode.childNodes[3].innerHTML;
    username = name;
}

function updateChange() {
    /*var position = $("#select1").val();*/
    var changeType = $("#select2").val();
    var newPosition = $("#select3").val();
    var newType = $("#select4").val();
    $.ajax({
        url: "UpdatecServlet",
        type: "post",
        async: "true",
        dataType: "json",
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data: {
            "username": username,
            "changeType": changeType,
            "newPosition": newPosition,
            "newType": newType
        },
        success: function (data) {

            alert(data.message);
            getinfo(0);
        }
    })
}