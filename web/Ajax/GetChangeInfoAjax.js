$(document).ready(function () {
        $.ajax({
            url:"GetChangeInfoServlet",
            type:"POST",
            async:true,
            catch:false,
            dataType:"json",//表示请求的返回数据格式{"username":"sss","password":"111"}
            contentType:"application/x-www-form-urlencoded;charset=utf-8",
            data:{

            },
            success:function (data) {//回调函数
                for (var i=0;i<data.length;i++){
                    $(".d3-img").eq(i).attr("src",data[i].headimg);
                    $(".d3-p1").eq(i).html(data[i].username);
                    var str =data[i].info;
                    str = str.replace("升迁",
                        '<span style="color:orange;">升迁</span>');
                    str = str.replace("转正",
                        '<span style="color:#a94442;">转正</span>');
                    str = str.replace("平调",
                        '<span style="color:salmon;">平调</span>');
                    str = str.replace("退休",
                        '<span style="color:rebeccapurple;">退休</span>');
                    str = str.replace("离职",
                        '<span style="color:red;">离职</span>');

                    $(".d3-p2").eq(i).html(str);
                    $(".d3-p3").eq(i).html(data[i].changetime);
                }
            },
            error:function () {
                alert("changeInfo失败")
            }
        })

    }
)