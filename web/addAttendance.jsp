<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2019/10/23
  Time: 14:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加考勤</title>
    <!-- 指定字符集 -->
    <meta charset="utf-8">
    <!-- 使用Edge最新的浏览器的渲染方式 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- viewport视口：网页可以根据设置的宽度自动进行适配，在浏览器的内部虚拟一个容器，容器的宽度与设备的宽度相同。
    width: 默认宽度与设备的宽度相同
    initial-scale: 初始的缩放比，为1:1 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>添加用户</title>

    <!-- 1. 导入CSS的全局样式 -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- 2. jQuery导入，建议使用1.9以上的版本 -->
    <script src="js/jquery-2.1.0.min.js"></script>
    <!-- 3. 导入bootstrap的js文件 -->
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">
        var classArray=[];
        classArray[0]=["请选择考勤分类"];
        classArray[1]=["请选择考勤分类","调休","病假","产假","事假"];
        classArray[2]=["请选择考勤分类","迟到","早退"];
        classArray[3]=["请选择考勤分类","出差"];
        classArray[4]=["请选择考勤分类","加班"];
        classArray[5]=["请选择考勤分类","旷工"];

        function classChanged(){
            var classObj=document.getElementById("s2");
            var oriObj=document.getElementById("s1");
            var data=classArray[oriObj.selectedIndex];

            while(classObj.childNodes.length>0){
                classObj.removeChild(classObj.lastChild);
            }

            for(var i=0;i<data.length;i++){
                var optionObj=document.createElement("option");
                optionObj.value=data[i];
                optionObj.innerHTML=data[i];
                classObj.appendChild(optionObj);


                /*
                 var optionObj=new Option(data[i],i);
                 classObj.options[i]=optionObj;
                 */
            }
        }



    </script>
</head>
<body>
<div class="container">
    <center><h3>添加员工考勤</h3></center>

    <form action="${pageContext.request.contextPath}/addAttendanceServlet" method="post"
          style="width: 400px;margin: auto;">
        <div class="form-group">
            <label for="name">姓名：</label>
            <input type="text" class="form-control" id="name" name="sName"
                   placeholder=" 请输入姓名">
        </div>

        <div class="form-group">
            <label>所在部门</label>
            <select class="form-control" name="sDepartment">
                <option style="display: none;">请选择部门</option>
                <option>设计部</option>
                <option>美术部</option>
                <option>市场部</option>
                <option>研发部</option>
            </select>
        </div>

        <div class="form-group">
            <label>考勤分类</label>
            <select class="form-control" name="sLeaveclass" id="s1" onchange="classChanged()">
                <option style="display: none;">请选择考勤分类</option>
                <option>请假</option>
                <option>迟到/早退</option>
                <option>出差</option>
                <option>加班</option>
                <option>旷工</option>
            </select>
        </div>

        <div class="form-group">
            <label>考勤类型</label>
            <select class="form-control" name="sLeavetype" id="s2">
                <option style="display: none;">请选择考勤类型</option>
            </select>
        </div>

        <div class="form-group">
            <label>开始时间</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="date" class="form-control pull-right lte-date"
                       id="d4311" value="" name="starttime">
            </div>
        </div>

        <div class="form-group">
            <label>结束时间</label>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input type="date" class="form-control pull-right lte-date"
                       id="d4312" value="" name="endtime">
            </div>
        </div>





        <div class="form-group" style="text-align: center">
            <input class="btn btn-primary" type="submit" value="添加"/>
            <input class="btn btn-default" type="reset" value="重置"/>
            <input class="btn btn-default" type="button" value="返回" onclick="loadPage('attendance.jsp')"/>
        </div>

        <%--        <div class="alert alert-warning alert-dismissible" role="alert">--%>
        <%--            <button type="button" class="close" data-dismiss="alert">--%>
        <%--                <span>&times;</span>--%>
        <%--            </button>--%>
        <%--            <strong style="align-content: center">${requestScope.addUserInfo}</strong>--%>
        <%--        </div>--%>
    </form>
</div>
</body>
</html>
