<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 2019/10/22
  Time: 19:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>人事变动</title>
    <link rel="stylesheet" type="text/css" href="css/test.css" />
    <link rel="stylesheet"
          href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/leavetest.css" />
    <link rel="stylesheet" type="text/css" href="css/leavetest01.css" />

    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script
            src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style type="text/css">
        .progress {
            width: 75%;
            height: 20px;
            background: #615f5f;
            padding: 5px;
            overflow: visible;
            border-radius: 15px;
            border-top: 1px solid #000;
            border-bottom: 1px solid #7992a8;
            float: left;
            margin-top: 10px;
        }

        .progress .progress-bar {
            border-radius: 15px;
            position: relative;
            animation: animate-positive 2s;
        }

        .progress .progress-value {
            display: block;
            padding: 3px 7px;
            font-size: 13px;
            color: #fff;
            border-radius: 4px;
            background: #191919;
            border: 1px solid #000;
            position: absolute;
            top: -40px;
            right: -10px;
        }

        .progress .progress-value:after {
            content: "";
            /* border-top: 10px solid #191919; */
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            position: absolute;
            bottom: -6px;
        }

        .progress-bar.active {
            animation: reverse progress-bar-stripes 0.40s linear infinite,
            animate-positive 2s;
        }

        .col-md-offset-3 {
            width: 95%;
            height: 204px;
            margin-left: 20px;
            text-align: center;
        }

        .progress-bar-danger {
            background-color: #4fd9c2
        }

        #users-info {
            margin-top: 10px;
            margin-left: 2%;
            margin-bottom: 5px;
        }
    </style>
</head>
<body>

<div role="tabpanel" class="tab-pane active" id="users-info">
    <div class="data-div">
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-xs-3">
                <input type="text" class="form-control" id="goodsCode"
                       placeholder="请输入员工名称">
            </div>
            <div class="col-xs-3">
                <select class="form-control select2" id="recommend"
                        data-placeholder="请选择部门" style="width: 100%;">
                    <option selected="selected" value="" style="display: none">请选择部门</option>
                    <option value="0">市场部</option>
                    <option value="1">设计部</option>
                    <option value="1">工程部</option>
                    <option value="1">会计部</option>
                </select>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" class="form-control pull-right lte-date"
                           id="d4311" value="" placeholder="开始时间">
                </div>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" class="form-control pull-right lte-date"
                           id="d4312" value="" placeholder="结束时间">
                </div>
            </div>
            <div class="col-xs-2">
                <button id="queryBtn" class="btn btn-primary my-btn">
                    <i class="fa fa-search button-in-i"></i>查询
                </button>
                <button id="clearBtn" class="btn btn-primary my-btn">
                    <i class="fa fa-trash button-in-i"></i>清空
                </button>
            </div>
        </div>
    </div>
</div>

<div class="herdertop">
    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#home" data-toggle="tab">转正</a></li>
        <li><a href="#late" data-toggle="tab">平调</a></li>
        <li><a href="#text1" data-toggle="tab">升迁</a></li>
        <li><a href="#text2" data-toggle="tab">退休</a></li>
        <li><a href="#text3" data-toggle="tab">离职</a></li>
    </ul>
</div>
<div class="costop">
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle"
                data-toggle="dropdown" style="border: none">
            最近一个月 <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu"
            style="min-width: 100%; background-color: #0de6a1">
            <li><a href="#">最近15天</a></li>
            <li class="divider"></li>
            <li><a href="#">最近30天</a></li>
        </ul>
    </div>
</div>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade in active" id="home">
        <table class="table_01">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">会计部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            请假类型 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">调休</a></li>
                            <li class="divider"></li>
                            <li><a href="#">事假</a></li>
                            <li class="divider"></li>
                            <li><a href="#">产假</a></li>
                            <li class="divider"></li>
                            <li><a href="#">病假</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img src="img/main_01.png"></td>
                <td>徐美图</td>
                <td>市场部</td>
                <td><span class="font_bk">调休</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_02.jpg"></td>
                <td>徐大志</td>
                <td>设计部</td>
                <td><span class="font_bk1">事假</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_03.jpg"></td>
                <td>黄小华</td>
                <td>市场部</td>
                <td><span class="font_bk">调休</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>已审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_04.jpg"></td>
                <td>杨玉环</td>
                <td>美工部</td>
                <td><span class="font_bk2">产假</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            </tbody>
        </table>

        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">
                <!-- <div class="n_left">调休</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-info progress-bar-striped active"
                            style="width: 85%;"></div>
                </div>
                <div class="p_left">10人</div>
                <!-- <div class="n_left">病假</div> -->
                <div class="progress">

                    <div
                            class="progress-bar progress-bar-success progress-bar-striped active"
                            style="width: 75%;"></div>
                </div>
                <div class="p_left">6人</div>
                <!-- <div class="n_left">产假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-warning progress-bar-striped active"
                            style="width: 90%;"></div>
                </div>
                <div class="p_left">2人</div>
                <!-- <div class="n_left">事假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-danger progress-bar-striped active"
                            style="width: 60%;"></div>

                </div>
                <div class="p_left">9人</div>
            </div>
        </div>
        <div class="cols02">
            <div id="col">各部门请假比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p>50%</p> 业务部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>30%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>15%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p>5%</p> 财务部
                    </td>
                </tr>
            </table>

        </div>
        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>

    </div>
    <div class="tab-pane fade" id="late">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">会计部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            出勤 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">迟到</a></li>
                            <li class="divider"></li>
                            <li><a href="#">早退</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img src="img/main_05.jpg"></td>
                <td>徐美图</td>
                <td>市场部</td>
                <td><span class="font_bk">迟到</span></td>
                <td>09/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_06.jpg"></td>
                <td>徐大志</td>
                <td>设计部</td>
                <td><span class="font_bk1">早退</span></td>
                <td>09/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_7.jpg"></td>
                <td>黄小华</td>
                <td>市场部</td>
                <td><span class="font_bk">迟到</span></td>
                <td>09/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_8.jpg"></td>
                <td>杨玉环</td>
                <td>美工部</td>
                <td><span class="font_bk2">早退</span></td>
                <td>09/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>

            </tbody>
        </table>


        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">
                <!-- <div class="n_left">调休</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-info progress-bar-striped active"
                            style="width: 85%;"></div>
                </div>
                <div class="p_left">10人</div>
                <!-- <div class="n_left">病假</div> -->
                <div class="progress">

                    <div
                            class="progress-bar progress-bar-success progress-bar-striped active"
                            style="width: 75%;"></div>
                </div>
                <div class="p_left">6人</div>
                <!-- <div class="n_left">产假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-warning progress-bar-striped active"
                            style="width: 90%;"></div>
                </div>
                <div class="p_left">2人</div>
                <!-- <div class="n_left">事假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-danger progress-bar-striped active"
                            style="width: 60%;"></div>

                </div>
                <div class="p_left">9人</div>
            </div>
        </div>

        <div class="cols02">
            <div id="col">各部门早退/迟到比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p>50%</p> 业务部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>30%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>15%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p>5%</p> 财务部
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>

    </div>
    <div class="tab-pane fade" id="text1">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            原职位 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">业务员1</a></li>
                            <li class="divider"></li>
                            <li><a href="#">业务员2</a></li>
                            <li class="divider"></li>
                            <li><a href="#">业务员3</a></li>
                            <li class="divider"></li>
                            <li><a href="#">业务员4</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            现职位 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">主管</a></li>
                            <li class="divider"></li>
                            <li><a href="#">经理</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计师</a></li>
                            <li class="divider"></li>
                            <li><a href="#">总监</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>

                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img src="img/main_05.jpg"></td>
                <td>徐美图</td>
                <td>业务员</td>
                <td><span class="font_bk">主管</span></td>
                <td>2016/07/09</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_06.jpg"></td>
                <td>徐大志</td>
                <td>设计师助理</td>
                <td><span class="font_bk1">设计师</span></td>
                <td>2016/07/09</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_7.jpg"></td>
                <td>黄小华</td>
                <td>设计师助理</td>
                <td><span class="font_bk">设计师</span></td>
                <td>2016/07/09</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_8.jpg"></td>
                <td>杨玉环</td>
                <td>业务员</td>
                <td><span class="font_bk2">主管</span></td>
                <td>2016/07/09</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>

            </tbody>
        </table>


        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">
                <!-- <div class="n_left">调休</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-info progress-bar-striped active"
                            style="width: 85%;"></div>
                </div>
                <div class="p_left">10人</div>
                <!-- <div class="n_left">病假</div> -->
                <div class="progress">

                    <div
                            class="progress-bar progress-bar-success progress-bar-striped active"
                            style="width: 75%;"></div>
                </div>
                <div class="p_left">6人</div>
                <!-- <div class="n_left">产假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-warning progress-bar-striped active"
                            style="width: 90%;"></div>
                </div>
                <div class="p_left">2人</div>
                <!-- <div class="n_left">事假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-danger progress-bar-striped active"
                            style="width: 60%;"></div>

                </div>
                <div class="p_left">9人</div>
            </div>
        </div>

        <div class="cols02">
            <div id="col">各部门加班比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p>50%</p> 业务部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>30%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>15%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p>5%</p> 财务部
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
    <div class="tab-pane fade" id="text2">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">会计部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            出差 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">出差</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img src="img/main_05.jpg"></td>
                <td>徐美图</td>
                <td>市场部</td>
                <td><span class="font_bk">出差</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_06.jpg"></td>
                <td>徐大志</td>
                <td>设计部</td>
                <td><span class="font_bk1">出差</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_7.jpg"></td>
                <td>黄小华</td>
                <td>市场部</td>
                <td><span class="font_bk">出差</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_8.jpg"></td>
                <td>杨玉环</td>
                <td>美工部</td>
                <td><span class="font_bk2">出差</span></td>
                <td>09/07/2016-11/07/2016</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>

            </tbody>
        </table>


        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">
                <!-- <div class="n_left">调休</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-info progress-bar-striped active"
                            style="width: 85%;"></div>
                </div>
                <div class="p_left">10人</div>
                <!-- <div class="n_left">病假</div> -->
                <div class="progress">

                    <div
                            class="progress-bar progress-bar-success progress-bar-striped active"
                            style="width: 75%;"></div>
                </div>
                <div class="p_left">6人</div>
                <!-- <div class="n_left">产假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-warning progress-bar-striped active"
                            style="width: 90%;"></div>
                </div>
                <div class="p_left">2人</div>
                <!-- <div class="n_left">事假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-danger progress-bar-striped active"
                            style="width: 60%;"></div>

                </div>
                <div class="p_left">9人</div>
            </div>
        </div>

        <div class="cols02">
            <div id="col">各部门出差比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p>50%</p> 业务部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>30%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>15%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p>5%</p> 财务部
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
    <div class="tab-pane fade" id="text3">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">会计部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            旷工 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">旷工</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><img src="img/main_05.jpg"></td>
                <td>徐美图</td>
                <td>市场部</td>
                <td><span class="font_bk">旷工</span></td>
                <td>2016/07/09</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_06.jpg"></td>
                <td>徐大志</td>
                <td>设计部</td>
                <td><span class="font_bk1">旷工</span></td>
                <td>2016/07/09</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_7.jpg"></td>
                <td>黄小华</td>
                <td>市场部</td>
                <td><span class="font_bk">旷工</span></td>
                <td>2016/07/09</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>
            <tr>
                <td><img src="img/main_8.jpg"></td>
                <td>杨玉环</td>
                <td>美工部</td>
                <td><span class="font_bk2">旷工</span></td>
                <td>2016/07/09</td>
                <td>待审核</td>
                <td>
                    <a href="#">审核</a>
                    <a href="#">编辑</a>
                    <a href="#">删除</a>
                </td>
            </tr>

            </tbody>
        </table>


        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">
                <!-- <div class="n_left">调休</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-info progress-bar-striped active"
                            style="width: 85%;"></div>
                </div>
                <div class="p_left">10人</div>
                <!-- <div class="n_left">病假</div> -->
                <div class="progress">

                    <div
                            class="progress-bar progress-bar-success progress-bar-striped active"
                            style="width: 75%;"></div>
                </div>
                <div class="p_left">6人</div>
                <!-- <div class="n_left">产假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-warning progress-bar-striped active"
                            style="width: 90%;"></div>
                </div>
                <div class="p_left">2人</div>
                <!-- <div class="n_left">事假</div> -->
                <div class="progress">
                    <div
                            class="progress-bar progress-bar-danger progress-bar-striped active"
                            style="width: 60%;"></div>

                </div>
                <div class="p_left">9人</div>
            </div>
        </div>

        <div class="cols02">
            <div id="col">各部门旷工比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p>50%</p> 业务部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>30%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p>15%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p>5%</p> 财务部
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
</div>
<script>
    $(function () {
        $('#myTab li:eq(1) a').tab('show');
    });
</script>


</body>
</html>
