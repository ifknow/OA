<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="./img/ff.png" type="image/x-icon">
    <title>精细化管理平台</title>
    <!--1.1导入bootcss.css-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <!-- 小图标 -->
    <link rel="stylesheet" type="text/css" href="css/fontawesome/css/font-awesome.min.css">
    <!-- date -->
    <link rel="stylesheet" type="text/css" href="css/datepicker/datepicker3.css">
    <!-- Select2 -->
    <link rel="stylesheet" type="text/css" href="css/select2/select2.min.css">
    <link rel="stylesheet" type="text/css" href="css/reset.css"/>
    <link rel="stylesheet" type="text/css" href="css/index06.css"/>
    <script type="text/javascript" src="Ajax/ModifyPasswordAjax.js"></script>
    <script type="text/javascript">
    </script>
</head>
<body>
<!--外壳 版心-->
<div id="jx-wrapper">
    <!--左侧栏-->
    <div id="left-menu" class="left-menu">
        <!--logo-->
        <div id="jx-logo">
            <p id="logob"><img id="logoa" alt="精细化平台" src="img/logoa.png"><span id="logoc">精细化管理平台</span></p>
        </div>
        <!--列表项-->
        <!--<div class="menu-title">首页</div>-->
        <div name="shouye" id="main.jsp" class="menu-item menu-item-active" href="javascript:void(0);">
            <img src="img/icon_source.png" id="homePage">首页
        </div>
        <div name="addUser.jsp" id="employeemanage.jsp" class="menu-item" href="javascript:void(0);">
            <img src="img/icon_chara_grey.png">员工信息管理
        </div>
        <div name="shouye" id="attendance.jsp" class="menu-item" href="javascript:void(0);">
            <img src="img/icon_user_grey.png">员工考勤
        </div>
        <div name="shouye" id="change.jsp" class="menu-item" href="#users-change">
            <img src="img/icon_change_grey.png">人事变动
        </div>
        <div name="shouye" class="menu-item" href="#help">
            <img src="img/icon_rule_grey.png">帮助
        </div>

    </div>
    <!--右侧栏-->
    <div id="right-content">
        <!--右侧导航-->
        <div class="right-nav">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                </div>
                <!--消息-->
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1" style="text-align: right;">
                    <!--气泡-->
                    <div class="messages">
                        <span id="message-bg"></span>
                        <span id="message-number">4</span>
                    </div>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 ">
                    <!--管理员-->
                    <div class="dropdown" style="min-width: 150px;">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="text-decoration: none;">
									<span id="u-name" style="font-weight: bolder; color: white;">
                                        ${sessionScope.user.username}，欢迎您！
									</span>
                            <b class="caret" style="color: white;"></b>
                        </a>
                        <div class="u-header" style="width: 60px;">
                            <img src="${sessionScope.user.headimg}" style="border-radius: 50%;width: 50px;height: 50px;
                                position: absolute; right: 2%; top: 10px;"/>
                        </div>
                        <ul class="dropdown-menu">
                            <li>
                                <a id="modified" data-toggle="modal" data-target="#modify"
                                   style="color: black;padding-left:20px;cursor: pointer;text-decoration: none;">修改密码</a>
                            </li>
                            <li><a href="loginOutServlet">退出登陆</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 ">
                    <!--add -->
                    <div id="add" name="add"></div>
                </div>
            </div>
        </div>
        <%--修改密码弹框↓↓↓↓↓↓↓--%>
        <div class="modal fade" id="modify" role="dialog" aria-labelledby="modifyModalLabel" style="">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="modifyModalLabel">修改密码</h4>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <form class="form-horizontal" action="">
                                <div class="form-group ">
                                    <label for="oldPassword" class="col-xs-3 control-label">原密码：</label>
                                    <div class="col-xs-8 ">
                                        <input type="text" name="oldPassword" class="form-control input-sm duiqi"
                                               id="oldPassword" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="newPassword" class="col-xs-3 control-label">新密码：</label>
                                    <div class="col-xs-8">
                                        <input type="text" name="newPassword" class="form-control input-sm duiqi"
                                               id="newPassword" placeholder="">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
                        <button type="button" class="btn btn-xs btn-green" onclick="modifyPassword()">保 存</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <%--修改密码弹框↑↑↑↑↑↑↑↑↑--%>

        <!--内容Tabs-->
        <div class="content-wrapper">

        </div>

    </div>


</div>
</body>
<!--1.2 按顺序导入js bootcss.js是对jquery的一个工具库，所有现有jquery-->
<script src="js/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
<!-- Select2 -->
<script src="css/select2/select2.full.min.js" type="text/javascript" charset="utf-8"></script>
<!-- bootstrap datepicker -->
<script src="css/datepicker/bootstrap-datepicker.js" type="text/javascript" charset="utf-8"></script>
<!-- date-range-picker -->
<!-- 	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js" type="text/javascript" charset="utf-8">></script> -->
<!-- <script src="css/daterangepicker/daterangepicker.js"></script> -->
<!-- bootstrap datetimepicker -->
<script src="css/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript" charset="utf-8"></script>
<script src="css/datetimepicker/bootstrap-datetimepicker.zh-CN.js" type="text/javascript" charset="utf-8"></script>
<!-- bootstrap time picker -->
<script src="css/timepicker/bootstrap-timepicker.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/common.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(function () {
        $(".menu-item").click(function () {
            console.log('click');
            $(".menu-item").removeClass("menu-item-active");
            $(this).addClass("menu-item-active");
            var itmeObj = $(".menu-item").find("img");
            itmeObj.each(function () {
                var items = $(this).attr("src");
                items = items.replace("_grey.png", ".png");
                items = items.replace(".png", "_grey.png")
                $(this).attr("src", items);
            });
            var attrObj = $(this).find("img").attr("src");
            ;
            attrObj = attrObj.replace("_grey.png", ".png");
            $(this).find("img").attr("src", attrObj);
        });

    });

    window.onload = function () {
        document.getElementById("homePage").onclick = loadPage("main.jsp");
    }

    $(function () {
        //给左侧列表栏，添加单击事件
        $(".menu-item").click(function () {
            //jQuery attr() 方法设置 + 号的 name 的属性值
            $("#add").attr("name", $(this).attr("name"));
            loadPage($(this).attr("id"));
        });

        $("#add").click(function () {
            if ($(this).attr("name") == "shouye") {
                alert("功能未开放！");
            } else {
                loadPage($(this).attr("name"));
            }
        });
    });
</script>
</html>
