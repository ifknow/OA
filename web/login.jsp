<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="./img/ff.png" type="image/x-icon">
    <title>用户登录</title>
    <!--1.1导入bootcss.css-->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/reset.css"/>
    <style type="text/css">

    </style>
    <link rel="stylesheet" type="text/css" href="css/login.css"/>
    <script type="text/javascript" src="js/Login.js"></script>
    <script type="text/javascript" src="js/Login3.js"></script>
    <script type="text/javascript" src="js/Login2.js"></script>
    <script>
        //切换验证码
        function refreshCode() {
            //1.获取验证码图片对象
            var vcode = document.getElementById("vcode");

            //2.设置其src属性，加时间戳
            vcode.src = "${pageContext.request.contextPath}/checkCodeServlet?time=" + new Date().getTime();
        }
    </script>
</head>
<body>
<form action="${pageContext.request.contextPath}/loginUserServlet" method="post">
    <div class="jx-l-wrapper">
        <div class="jx-l-header">
            <img src="img/logo.png"/>
        </div>
        <div class="jx-l-input">
            <h1 class="jx-l-t">精细化管理平台</h1>
            <div class="jx-l-user">
                <div class="jx-l-tt" style=""><span>账号登录</span></div>
                <div class="jx-l-ui">
                    <div id="u-name" class="jx-user">
                        <input class="u-input" type="text" placeholder="请输入手机号" name="phone" id="phone"
                               value="${phone}" title=" 请输入手机号"/>
                        <b style="background-image: url(img/uh.png);"></b>
                    </div>
                    <div id="u-pass" class="jx-user">
                        <input class="u-pass" type="password" placeholder="请输入密码" name="password" id="password"
                               value="${password}" title="请输入密码"/>
                        <b style="background-image: url(img/pw.png);"></b>
                    </div>
                    <div class="jx-table">
                        <div class="row">
                            <div class="cell">
                                <input class="form-control" id="verifycode" type="text" placeholder="请输入验证码"
                                       title="请输入验证码" name="verifycode" style="width: 140px;"/>
                            </div>
                            <div class="cell" style="width: 50%;">
                                <%--<span style="display: inline-block;width: 50px;background: yellow;height: 30px;line-height: 30px;">41045</span>--%>
                                <a href="javascript:refreshCode();">
                                    <img src="${pageContext.request.contextPath}/checkCodeServlet" id="vcode"
                                         alt="验证码" title="看不清点击刷新">
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="cell" style="color: #FFFFFF;">
                                <input type="checkbox" name="checkRename" id="checkRename"/><label title="点击按钮记住账号密码"
                                                                                                   for="checkRename">记住账号</label>
                            </div>
                            <div class="cell" style="padding-left: 22px;">
                                <input class="btn btn btn-default" type="submit" value="登录" id="btnlogin">
                            </div>
                        </div>


                    </div>
                </div>


            </div>
            <!-- 出错显示的信息框 -->
            <div class="alert alert-warning alert-dismissible" role="alert" style="width: 240px;margin: auto;">
                <button type="button" class="close" data-dismiss="alert">
                    <span>&times;</span>
                </button>
                <strong>${requestScope.msg}</strong>
            </div>
        </div>

        <div class="jx-l-footer">
            <div class="footer-content">
                版权所有 | 豫ICP备17070472号
            </div>
        </div>
    </div>
</form>
</body>
<!--1.2 按顺序导入js bootcss.js是对jquery的一个工具库，所有现有jquery-->
<script src="js/jquery-3.3.1.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
</html>

