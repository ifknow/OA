<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--<%
    HttpSession session1 = request.getSession();
    User user = (User) session1.getAttribute("maps");
%>--%>

<html>
<head>
    <meta charset="UTF-8">
    <title>创建员工信息</title>
    <style type="text/css">
        td {
            margin: 10px;
            padding: 10px;
        }

        fieldset, table {
            margin: 0 auto;
            text-align: center;
        }

    </style>
</head>
<body>
<div style="height: 100%;" id="body">
    <c:forEach items="${usermaps}" var="usermaps">
        <fieldset style="border: 0px solid black;">
            <legend style=" width: 100%;height: 40px;background-color: rgb(83, 149, 223);color: white;font-weight: bolder;
            font-size: 15px; margin: 0px auto;text-align: center; padding-top: 10px;">个人资料
            </legend>

            <table>

                <tr>
                    <td rowspan="5">
                        <img value="图片" src="${usermaps.headimg}" style="width: 150px; height: 180px;" disabled/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>姓名:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" name="name" value="${usermaps.username}" disabled>
                    </td>
                    <td>
                        <span>性别:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.sex}" name="sex" disabled>
                    </td>


                    <td>
                        <span>电话:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.phone}" name="tel" disabled>
                    </td>
                <tr>
                    <td>
                        <span>邮箱:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.email}" name="email" disabled>
                    </td>
                    <td>
                        <span>QQ:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.qq}" name="qq" disabled>
                    </td>
                    <td>
                        <span>身份证号:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.uid}" name="uid" disabled>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span>家庭住址:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.address}" name="address" disabled>
                    </td>

                    <td>
                        <span>入职时间:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.entrytime}" name="entrytime" disabled>
                    </td>
                    <td>
                        <span>部门:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.departmentName}" name="address"
                               disabled>
                    </td>
                <tr>
                    <td>
                        <span>职位:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.positionName}" name="address"
                               disabled>
                    </td>
                </tr>
                </tr>


            </table>
        </fieldset>
        <fieldset>
            <legend  style=" width: 100%;height: 40px;background-color: rgb(83, 149, 223);color: white;font-weight: bolder;
            font-size: 15px; margin: 0px auto;text-align: center; padding-top: 10px;">职位变动</legend>
            <table>
                <tr>
                    <td>
                        <span>原部门:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.olddempartmentName}" name="address"
                               disabled>
                    </td>
                    <td>
                        <span>原职位:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.oldpositionName}" name="address"
                               disabled>
                    </td>
                    <td>
                        <input class="form-control" value="${usermaps.type}" disabled
                               style="width: 80px;font-weight: bolder;"></input>
                    </td>
                    <td>
                        <span>现部门:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.newdepartmentName}" name="address"
                               disabled>
                    </td>
                    <td>
                        <span>现职位:</span>
                    </td>
                    <td>
                        <input class="form-control" type="text" value="${usermaps.newpositionName}" name="address"
                               disabled>
                    </td>
                </tr>

            </table>
        </fieldset>
    </c:forEach>
    <div class="form-group" style="text-align: center;padding-top: 80px;">
        <input class="btn btn-default" type="button" value="返回" onclick="loadPage('employeemanage.jsp')"/>
    </div>
</div>
</body>
</html>

<%--
    </c:otherwise>
</c:choose>
--%>
