<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html style="height: 100%">
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <script type="text/javascript" src="js/echarts.js"></script>
    <script type="text/javascript" src="Ajax/GetRecentInfoAjax.js"></script>
    <script type="text/javascript" src="Ajax/GetChangeInfoAjax.js"></script>
    <script type="text/javascript" src="Ajax/PieChartAjax.js"></script>
    <script type="text/javascript" src="Ajax/HistogramAjax.js"></script>
    <script type="text/javascript" src="Ajax/OperationInfoAjax.js"></script>
    <script type="text/javascript" src="Ajax/ExamineInfoAjax.js"></script>

    <script type="text/javascript">
        $(function () {
            var date = new Date();
            var datestr = date.toLocaleDateString();
            datestr = datestr.replace(/年/g, ",")
            datestr = datestr.replace(/月/g, ",")
            datestr = datestr.replace(/日/g, ",")
            datestr = datestr.substring(0, datestr.lastIndexOf(","))
            // Date picker 时间插件
            $('#d4311').datetimepicker({
                language: 'zh-CN',
                autoclose: true,
                minView: "month",//设置只显示到月份
                format: 'yyyy-mm-dd',
                maxDate: new Date(),
                minDate: new Date(2020, 10 - 1, 25),
            });

            $('#d4312').datetimepicker({
                language: 'zh-CN',
                autoclose: true,
                minView: "month",//设置只显示到月份
                format: 'yyyy-mm-dd',
            });

            $(".select2").select2({
                allowClear: true
            });
        })
    </script>

</head>
<body>
<div class="d">
    <div class="d1">
        <table border="" cellspacing="0" cellpadding="0">
            <tr id="examine-tr">
                <td class="examine-td">
                    <span id="num1" class="examine-span">4</span>
                    <span id="span1">审核通过</span>
                </td>
                <td id="t1" class="examine-td">
                    <span id="num2" class="examine-span">88</span>
                    <span id="span2">审核未通过</span></td>
                <td id="t2" class="examine-td">
                    <span id="num3" class="examine-span">6</span>
                    <span id="span3">未审核</span>
                </td>
            </tr>
        </table>
    </div>
    <div class="d2">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr id="t3">
                <td colspan="2">最近录入员工</td>
            </tr>
            <tr>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg1" class="headimg"/>
                        <a href="" onclick="" id="a1" class="user-name"></a>
                    </div>
                </td>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg2" class="headimg"/>
                        <a href="" onclick="" id="a2" class="user-name"></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg3" class="headimg"/>
                        <a href="" onclick="" id="a3" class="user-name"></a>
                    </div>
                </td>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg4" class="headimg"/>
                        <a href="" onclick="" id="a4" class="user-name"></a>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg5" class="headimg"/>
                        <a href="" onclick="" id="a5" class="user-name"></a>
                    </div>
                </td>
                <td>
                    <div class="u-header">
                        <img src="" id="headImg6" class="headimg"/>
                        <a href="" onclick="" id="a6" class="user-name"></a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="d3">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr id="t4">
                <td>最近员工变动</td>
            </tr>
            <tr>
                <td class="d3-td">
                    <img src="" alt="" class="d3-img">
                    <a class="d3-p1"></a>
                    <p class="d3-p2"></p>
                    <p class="d3-p3"></p>
                </td>
            </tr>
            <tr>
                <td class="d3-td">
                    <div>
                        <div>
                            <img src="" alt="" class="d3-img">
                        </div>
                        <a class="d3-p1"></a>
                        <p class="d3-p2"></p>
                        <p class="d3-p3"></p></div>
                </td>
            </tr>
            <tr>
                <td class="d3-td">
                    <div>
                        <img src="" alt="" class="d3-img">
                        <a class="d3-p1"></a>
                        <p class="d3-p2"></p>
                        <p class="d3-p3"></p></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="d4">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr id="t5">
                <td>
                    <div class="pie" style="height: 150px;width: 100px"></div>
                    <div class="pie" style="height: 150px;width: 100px"></div>
                    <div class="pie" style="height: 150px;width: 100px"></div>
                    <div class="pie" style="height: 150px;width: 100px"></div>
                </td>
            </tr>
            <tr>
                <td id="t6">
                    <div class="histogram" style="height: 244px;width: 100px"></div>
                    <div class="histogram" style="height: 244px;width: 100px"></div>
                    <div class="histogram" style="height: 244px;width: 100px"></div>
                    <div class="histogram" style="height: 244px;width: 100px"></div>
                </td>
            </tr>
        </table>
    </div>
    <div class="d5">
        <img src="" alt="" class="operationimg">
        <p class="logintime"></p>
        <p class="info1"></p>
        <p class="info2"></p>
    </div>
    <div class="d6">
        <img src="" alt="" class="operationimg">
        <p class="logintime"></p>
        <p class="info1"></p>
        <p class="info2"></p>
    </div>
    <div class="d7">
        <select name="11" onchange="PieChart()" id="selectid">
            <option value="最近一个月">最近一个月</option>
            <option value="最近三天">最近三天</option>
            <option value="最近一周">最近一周</option>
        </select>
    </div>
    <div class="d8">
        <input type="button" value="clear" onclick="cleardate()"
               style="width: 50px;height: 20px;margin-right: 10px;margin-top:5px">
        <input type="text" class="form-control pull-right lte-date"
               id="d4311" value="" placeholder="结束时间" onfocus="WdatePicker({dateFmt:'yyyyMMdd',isShowWeek:true})"
               onchange="PieChart()">
        <input type="text" class="form-control pull-right lte-date"
               id="d4312" value="" placeholder="开始时间" onfocus="WdatePicker({dateFmt:'yyyyMMdd',isShowWeek:true})"
               onchange="PieChart()">
    </div>
    <p style="color: red;margin-left: 600px" id="date-message"></p>

</div>


</body>

</html>

