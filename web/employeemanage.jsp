<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
    <c:when test="${empty requestScope.key_userList}">
        <jsp:forward page="/showAllUserInfoServlet"></jsp:forward>
    </c:when>
    <c:otherwise>
        <html>
        <head>
            <meta charset="utf-8"/>
            <title></title>
            <style type="text/css">
            </style>
            <!-- 小图标 -->
            <link rel="stylesheet" type="text/css"
                  href="css/fontawesome/css/font-awesome.min.css"/>
            <!-- date -->
            <link rel="stylesheet" type="text/css"
                  href="css/datepicker/datepicker3.css"/>
            <!-- Select2 -->
            <link rel="stylesheet" type="text/css"
                  href="css/select2/select2.min.css"/>
            <script src="js/jquery-3.3.1.min.js"></script>
            <link href="css/employee.css" rel="stylesheet" type="text/css"/>

        </head>
        <body class="hold-transition skin-blue sidebar-mini">

        <section class="content">
            <div role="tabpanel" class="tab-pane active" id="users-info">
                <div class="data-div">
                    <form>
                        <div class="row" style="margin-bottom: 5px;">
                            <div class="col-xs-2" style="width: 300px;">
                                <input type="text" class="form-control" id="id222"
                                       placeholder="请输入员工名称">
                            </div>
                            <div class="col-xs-2" style="width: 300px;">
                                <select class="form-control select2"
                                        data-placeholder="请选择部门" style="width: 100%;" id="id223">
                                    <option selected="selected"></option>
                                    <option value="市场部">市场部</option>
                                    <option value="设计部">设计部</option>
                                    <option value="工程部">工程部</option>
                                    <option value="财务部">财务部</option>
                                </select>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" class="form-control pull-right lte-date"
                                           id="d4311" value="" placeholder="开始时间">
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="date" class="form-control pull-right lte-date"
                                           id="d4312" value="" placeholder="结束时间">
                                </div>
                            </div>
                            <div class="col-xs-2" style="width: 212px;float: right;padding-left: 15px;">
                                <a id="queryBtn"
                                   class="btn btn-primary my-btn"
                                   type="submit">
                                    <i class="fa fa-search button-in-i"></i>查询
                                </a>
                                <a id="clearBtn" class="btn btn-danger my-btn" type="reset"
                                   onclick="resetQuery()">
                                    <i class="fa fa-trash button-in-i"></i>清空
                                </a>
                                    <%--<a id="addBtn" class="btn btn-primary my-btn" value="添加"
                                       onclick="loadPage('addUser.jsp')">
                                        <i class="fa fa-cart-arrow-down button-in-i"></i>添加
                                    </a>--%>
                            </div>
                        </div>
                    </form>
                    <div style="text-align: center;" class="row tableHeader">
                        <div class="col-xs-1">头像</div>
                        <div class="col-xs-2">姓名</div>
                        <div class="col-xs-1">性别</div>
                        <div class="col-xs-3 ">
                            部门 <b class="active"></b>
                                <%--<select class="form-control select2" id="title"
                                        data-placeholder="按部门" style="width: 100%; text-align: center;">
                                    <option selected="selected" value="" style="display: none">按部门</option>
                                    <option value="经理">设计部</option>
                                    <option value="设计师">美术部</option>
                                    <option value="技术总监">市场部</option>
                                    <option value="主管">主管</option>
                                </select>--%>
                        </div>
                        <div class="col-xs-3">
                            职称 <b class="active"></b>
                                <%--<select class="form-control select2" id="title1"
                                        data-placeholder="按职称" style="width: 100%;">
                                    <option selected="selected" value="" style="display: none">按职位</option>
                                    <option value="经理">经理</option>
                                    <option value="设计师">设计师</option>
                                    <option value="技术总监">技术总监</option>
                                    <option value="主管">主管</option>
                                </select>--%>
                        </div>
                        <div class="col-xs-2">操作</div>
                    </div>
                    <div style="text-align: center;" class="table-body">
                        <c:forEach items="${key_userList}" var="userr">
                            <div class="row" id="hoverrow">
                                <div class="col-xs-1">
                                    <img id="image" src="${userr.headimg}"
                                         style="width: 50px;height: 50px;border-radius: 30%;"/>
                                </div>
                                <div class="col-xs-2" id="username">${userr.username}</div>
                                <div class="col-xs-1" id="sex">${userr.sex}</div>
                                <div class="col-xs-3" id="departmentName">${userr.departmentName}</div>
                                <div class="col-xs-3" id="positionName">${userr.positionName}</div>
                                <div class="col-xs-4" style="width: 200px;float:right;">
                                    <!--隐藏域存放员工Id-->
                                    <div hidden>${userr.id}</div>
                                        <%--<a class="btn btn-success btn-xs"
                                           onclick="loadPage('viewInformation.jsp',{'sid':'${userr.id}'})">查看</a>--%>
                                    <a class="btn btn-success btn-xs"
                                       onclick="loadPage('viewInformation?id=${userr.id}')">查看</a>

                                    <a class="btn btn-primary btn-xs"
                                       onclick="loadPage('eidtUserServlet?id=${userr.id}')">编辑</a>

                                        <%--<a class="btn btn-success btn-xs"
                                           href="javascript:EidtUserServlet(${userr.id})">编辑</a>--%>

                                    <a class="btn btn-danger btn-xs"
                                       href="javascript:deleteUser(${userr.id})">删除</a>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <!--页码块-->
                <div id="pagenumber">
                    <nav aria-label="Page navigation">
                        <ul class="pagination">
                            <li class="pageli"><a onclick="loadPage('switchPageServlet?info=首页')" title="返回到首页">首页</a>
                            </li>
                            <li class="pageli">
                                <a onclick="loadPage('switchPageServlet?info=上一页&pageNumber=${requestScope.pageNumber }')"
                                   title="切换下一页">上一页</a>
                            </li>
                            <li class="pageli">
                                <a onclick="loadPage('switchPageServlet?info=下一页&pageNumber=${requestScope.pageNumber }')"
                                   title="返回上一页">下一页</a>
                            </li>
                            <li class="pageli"><a onclick="loadPage('switchPageServlet?info=尾页')" title="跳转到尾页">尾页</a>
                            </li>
                            <span>
                                第 ${requestScope.pageNumber } 页
                                共 ${requestScope.pagetotalNumber } 页，共 ${requestScope.totalNumber } 条记录
                            </span>

                        </ul>
                    </nav>
                </div>
            </div>
        </section>
        </body>

        <script type="text/javascript">
            function deleteUser(id) {
                if (confirm("是否要删除？")) {
                    $.get("delUserServlet",
                        {id: id},
                        function (data) {
                            if (data.tt == 1) {
                                loadPage('employeemanage.jsp');
                            }
                        }, "json");
                }
            }

            function eidtUser(username) {
                location.href = "${pageContext.request.contextPath}/eidtUserServlet?username=" + username;

            }

            function resetQuery() {
                var name = document.getElementById("id222");
                var department = document.getElementById("id223");
                var startTime = document.getElementById("id4311");
                var overTime = document.getElementById("id4312");
                $("#id222").val("");
                $("#id223").empty();
                $("#id4311").val("");
                $("#id4312").val("");
            }

            $(function () {
                $("#queryBtn").click(function () {
                    var info = "searchname=" + $("#id222").val() + "&recommend=" + $("#id223 option:selected").val()
                        + "&starttime=" + $("#d4311").val() + "&endtime=" + $("#d4312").val();
                    console.log(info);
                    loadPage('selectUserBySearchServlet?' + info);
                })
            })

            function viewInformation(id) {
                // alert(id);
                $.ajax({
                    url: "viewInformation.jsp",
                    type: "post",
                    async: true,
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data: {
                        id: id,
                    },
                    //请求成功后，回调函数
                    success: function (data) {

                    }
                })
            }

            function EidtUserServlet(id) {
                location.href = "${pageContext.request.contextPath}/eidtUserServlet?id=" + id;
            }
        </script>
        </html>
    </c:otherwise>
</c:choose>