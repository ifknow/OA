/**
 * 记住密码
 */
window.onload = function () {
    //手机号
    var phone = document.getElementById("phone");
    //密码
    var password = document.getElementById("password");
    //多选框
    var checkRember = document.getElementById("checkRename");
    //登录
    var btnLogin = document.getElementById("btnlogin");

    if (getCookie("phone") && getCookie("password")) {
        phone.value = getCookie("phone");
        password.value = getCookie("password");
        checkRember.checked = true;
    }
    //多选框状态
    checkRember.onchange = function () {
        //没选中全删
        if (!this.checked) {
            delCookie("phone");
            delCookie("password");
        }
    }

    //单击事件，点中就存入文本框
    btnLogin.onclick = function () {
        if (checkRember.checked) {
            setCookie("phone", phone.value, 7);
            setCookie("password", password.value, 7);
        }
    }
}

//根据传入的cookie的键的名称，获取对应的值
function getCookie(name) {
    var rag = RegExp(name + "=([^;])+");
    var arr = document.cookie.match(rag);
    if (arr) {
        var s = arr[0].split("=");
        return unescape(s[1]);
    } else {
        return "";//没找到返回空字符串
    }
    /*var value;
    var cookieStr = document.cookie;
    //判断是否存在cookie
    if (cookieStr.length > 0) {
        //判断是否存在我们要找的东西
        var s_start = cookieStr.indexOf(name + "=");
        if (s_start != -1) {
            //username=javaee; password=javaee
            s_start = s_start + name.length + 1;
            s_end = cookieStr.indexOf(";", s_start);
            if (s_end == -1) {
                //只写了一个cookie的清况
                //username=javaee
                s_end = cookieStr.length;
            }
            value = cookieStr.substring(s_start, s_end);
        }
    }
    return value;*/
}

//将值存入cookie中
function setCookie(name, value, day) {
    //创建时间对象
    var now = new Date();
    //多长时间过期
    now.setDate(now.getDate() + day);
    //escape编码格式
    document.cookie = name + "=" + escape(value) + ";expries" + now;

}

//删除cookie
function delCookie(name) {
    setCookie(name, null, -1);
}