<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>编辑用户</title>
    <script>
        function updateUser() {
            console.log($("#deparement option:selected").val());
            console.log($("#position option:selected").val());
            alert("信息修改成功！");
            $.ajax({
                url: "updateUserServlet",
                type: "post",
                async: true,
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {

                    id: $("#iid").val(),
                    headimg: $("#headimg").val(),
                    name: $("#name").val(),
                    sex: $("#sex").val(),
                    tel: $("#tel").val(),
                    email: $("#email").val(),
                    qq: $("#qq").val(),
                    uid: $("#uid").val(),
                    address: $("#address").val(),
                    department: $("#deparement option:selected").val(),
                    position: $("#position option:selected").val(),
                    entrytime: $("#entrytime").val(),
                },
                //请求成功后，回调函数
                success: function (data) {

                    loadPage("eidtUser.jsp");
                }
            })
        }


        function show(file) {
            //实例化一个FileReader对象，读取文件
            var reader = new FileReader();
            //获取要显示图片的标签
            var img = document.getElementById('img');
            //读取File对象的数据
            reader.onload = function (evt) {
                img.width = "140";
                img.height = "160";
                img.src = evt.target.result;
            }
            reader.readAsDataURL(file.files[0]);
        }
    </script>
    <style>
        td {
            padding: 10px;
        }
    </style>
</head>
<body>
<div class="container">

    <%--    <form method="post">--%>
    <%--    <form action="${pageContext.request.contextPath}/updateUserServlet" method="post">--%>

    <c:forEach items="${usermapss}" var="usermapss">
        <div style="width: 100%;">
            <fieldset>
                <legend style="text-align: center;margin-top: 150px;">员工基本信息
                </legend>
                <table>
                    <tr>
                        <td rowspan="5" style="border: 1px solid black;width: 165px;">
                            <input id="headimg" value="${usermapss.headimg}" hidden></input>
                            <div>
                                <img alt="图片" id="img" src="${usermapss.headimg}"
                                     style="width: 130px; height: 160px;padding-left: 6px;">
                            </div>
                            <form action="fileUploadServlet" method="post" enctype="multipart/form-data">
                                <input type="text" name="${usermapss.id}" hidden>
                                <input type="file" name="myfile" id="myfile"
                                       style="width: 120px;padding-left: 10px;padding-top: 10px;" onchange="show(this)"><br>
                                <input type="submit" value="点击更换头像" style="padding-left: 10px;">
                            </form>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>姓名:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" name="name" value="${usermapss.username}" id="name">
                        </td>
                        <input type="text" name="id" value="${usermapss.id}" id="iid" hidden>
                        <td>
                            <span>性别:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.sex}" name="sex" id="sex">
                        </td>
                        <td>
                            <span>电话:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.phone}" name="tel" id="tel">
                        </td>
                    <tr>
                        <td>
                            <span>邮箱:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.email}" name="email" id="email">
                        </td>
                        <td>
                            <span>QQ:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.qq}" name="qq" id="qq">
                        </td>
                        <td>
                            <span>身份证号:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.uid}" name="uid" id="uid">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>家庭住址:</span>
                        </td>
                        <td>
                            <input class="form-control" type="text" value="${usermapss.address}" name="address" id="address">
                        </td>

                        <td>
                            <span>入职时间:</span>
                        </td>
                        <td>
                            <input class="form-control" type="date" value="${usermapss.entrytime}" name="entrytime" id="entrytime">
                        </td>
                        <td>
                            <span>部门:</span>
                        </td>
                        <td>
                                <%--<input type="text" value="${usermapss.departmentName}" name="department" id="department">--%>
                            <select class="form-control" name="department"
                                    id="deparement">
                                <option>${usermapss.departmentName}</option>
                                <option>设计部</option>
                                <option>工程部</option>
                                <option>财务部</option>
                                <option>市场部</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span>职位:</span>
                        </td>
                        <td>
                                <%--<input type="text" value="${usermapss.positionName}">--%>
                            <select class="form-control" name="position"
                                    id="position">
                                <option>${usermapss.positionName}</option>
                                <option>设计师</option>
                                <option>技术总监</option>
                                <option>主管</option>
                                <option>经理</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </fieldset>
        </div>

    </c:forEach>

    <div class="form-group" style="text-align: center">
        <input class="btn btn-primary" type="button" onclick="updateUser()" value="保存"/>
        <%--<input class="btn btn-default" type="reset" value="重置"/>--%>
        <input class="btn btn-default" type="button" value="返回" onclick="loadPage('employeemanage.jsp')"/>
    </div>

    <%--<div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert">
            <span>&times;</span>
        </button>
        <strong>${requestScope.eidtUserMsg}</strong>
    </div>
--%>
    <%-- </form>--%>

</div>
</body>
</html>
