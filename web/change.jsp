<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>人事变动</title>
</head>
<link rel="stylesheet" type="text/css" href="css/test.css"/>
<link rel="stylesheet"
      href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/leavetest.css"/>
<link rel="stylesheet" type="text/css" href="css/leavetest01.css"/>

<script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript" src="Ajax/ShowUser.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="Ajax/Updatec.js"></script>
<script>
    $(function () {
        $('#myTab li:eq(1) a').tab('show');
    });


    $(document).ready(function () {
        $("#clearBtn").click(function () {
            $("#goodsCode").val("");
            $("#recommend").val("");
            $("#d4311").val("");
            $("#d4312").val("");
        })
    })
</script>

<style type="text/css">
    .attend {
        line-height: 100%;
        width: 15%;
        height: 20px;
        padding: 5px;
        overflow: visible;
        float: left;
        margin-top: 10px;
    }

    .progress {
        width: 70%;
        height: 20px;
        background: #615f5f;
        padding: 5px;
        overflow: visible;
        border-radius: 15px;
        border-top: 1px solid #000;
        border-bottom: 1px solid #7992a8;
        float: left;
        margin-top: 5px;

    }

    .progress .progress-bar {
        border-radius: 15px;
        position: relative;
        animation: animate-positive 2s;
    }

    .progress .progress-value {
        display: block;
        padding: 3px 7px;
        font-size: 13px;
        color: #fff;
        border-radius: 4px;
        background: #191919;
        border: 1px solid #000;
        position: absolute;
        top: -40px;
        right: -10px;
    }

    .progress .progress-value:after {
        content: "";
        /* border-top: 10px solid #191919; */
        border-left: 10px solid transparent;
        border-right: 10px solid transparent;
        position: absolute;
        bottom: -6px;
    }

    .progress-bar.active {
        animation: reverse progress-bar-stripes 0.40s linear infinite,
        animate-positive 2s;
    }

    .col-md-offset-3 {
        width: 95%;
        height: 204px;
        margin-left: 20px;
        text-align: center;
    }

    .progress-bar-danger {
        background-color: #4fd9c2
    }

    #users-info {
        margin-top: 10px;
        margin-left: 2%;
        margin-bottom: 5px;
    }

    .tbody img {
        height: 40px;
        width: 40px;
        border-radius: 80%;
    }

    .homq1 {
        width: 100%;
    }

    .table_001 {
        width: 94.5%;
        border-spacing: 0 2.5px;
        float: left;
        border-collapse: separate;
        margin-left: 2%;
        margin-top: 10px;
        position: relative;
    }

    .table_001 td {
        width: 1040px;
        background-color: white;
        vertical-align: middle;
        text-align: center;
    }

    .table_001 thead tr {
        height: 50px;
    }

    .table_001 tbody tr {
        height: 87px;
    }

    a {
        text-decoration: none;
        color: cornflowerblue;
    }

    img {
        vertical-align: middle;
    }

    #gridSystemModalLabel {
        text-align: right;
    }

    .modal-content1 {
        background-color: silver;
    }

    .div1 {
        position: absolute;
        left: 10px;
    }

    .select4 {
        width: 300px;
        height: 37px;
    }

    #div4 {
        position: absolute;
        right: 50px;
        top: 720px;
    }

    .boxs {
        float: left;
        /*position: absolute;
        right: 20px;
        top: 137px;*/
    }
</style>
<body>
<div role="tabpanel" class="tab-pane active" id="users-info">
    <div class="data-div">
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-xs-3">
                <input type="text" class="form-control" id="goodsCode" name="username"
                       placeholder="请输入员工名称">
            </div>
            <div class="col-xs-3">
                <select class="form-control select2" id="recommend"
                        data-placeholder="请选择部门" style="width: 100%;">
                    <option selected="selected" value="" style="display: none">请选择部门</option>
                    <option value="市场部">市场部</option>
                    <option value="设计部">设计部</option>
                    <option value="工程部">工程部</option>
                    <option value="财务部">财务部</option>
                </select>
            </div>
            <%--    <div class="col-xs-2">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right lte-date"
                               id="d4311" value="" placeholder="开始时间">
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="date" class="form-control pull-right lte-date"
                               id="d4312" value="" placeholder="结束时间">
                    </div>
                </div>--%>
            <div class="boxs" style="width: 212px;float: right;padding-left: 15px;">
                <button id="queryBtn" class="btn btn-primary my-btn" onclick="getinfo()">
                    <i class="fa fa-search button-in-i"></i>查询
                </button>
                <button id="clearBtn" class="btn btn-danger my-btn">
                    <i class="fa fa-trash button-in-i"></i>清空
                </button>
            </div>
        </div>
    </div>
</div>
<%--<div id="myTabContent" class="tab-content">--%>
<div class="homq1" id="home">
    <table class="table_001">
        <thead>
        <tr>
            <td>相片</td>
            <td>姓名</td>
            <td>部门</td>
            <td>职称</td>
            <td>日期</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody class="tbody">
        <tr class="tr1">
            <td><img src="" class="changes-img"></td>
            <td class="changes-username">徐美图</td>
            <td class="changes-departmentname">部门</td>
            <td class="changes-positionname"><span class="font_bk">职称</span></td>
            <td class="changes-time">09/07/2016-11/07/2016</td>
            <td>
                <div class="modified" data-toggle="modal" data-target="#modify1">
                    <a style="color: white;font-weight: bolder;width: 60px;height: 23px;"
                       class="btn btn-primary btn-xs" onclick="update(this)">编辑</a>
                </div>
            </td>
        </tr>
        <tr class="tr1">
            <td><img src="" class="changes-img"></td>
            <td class="changes-username">徐美图</td>
            <td class="changes-departmentname">市场部</td>
            <td class="changes-positionname"><span class="font_bk">调休</span></td>
            <td class="changes-time">09/07/2016-11/07/2016</td>
            <td>
                <div class="modified" data-toggle="modal" data-target="#modify1">
                    <a style="color: white;font-weight: bolder;width: 60px;height: 23px;" class="btn btn-primary btn-xs"
                       onclick="update(this)">编辑</a>
                </div>
            </td>
        </tr>
        <tr class="tr1">
            <td><img src="" class="changes-img"></td>
            <td class="changes-username">徐美图</td>
            <td class="changes-departmentname">市场部</td>
            <td class="changes-positionname"><span class="font_bk">调休</span></td>
            <td class="changes-time" >09/07/2016-11/07/2016</td>
            <td>
                <div class="modified" data-toggle="modal" data-target="#modify1">
                    <a style="color: white;font-weight: bolder;width: 60px;height: 23px;"
                       class="btn btn-primary btn-xs" onclick="update(this)">编辑</a>
                </div>
            </td>
        </tr>
        <tr class="tr1">
            <td><img src="" class="changes-img"></td>
            <td class="changes-username">徐美图</td>
            <td class="changes-departmentname">市场部</td>
            <td class="changes-positionname"><span class="font_bk">调休</span></td>
            <td class="changes-time">09/07/2016-11/07/2016</td>
            <td>
                <div class="modified" data-toggle="modal" data-target="#modify1">
                    <a style="color: white;font-weight: bolder;width: 60px;height: 23px;"
                       class="btn btn-primary btn-xs" onclick="update(this)">编辑</a>
                </div>
            </td>
        </tr>
        <tr class="tr1">
            <td><img src="" class="changes-img"></td>
            <td class="changes-username">徐美图</td>
            <td class="changes-departmentname">市场部</td>
            <td class="changes-positionname"><span class="font_bk">调休</span></td>
            <td class="changes-time">09/07/2016-11/07/2016</td>
            <td>
                <div class="modified" data-toggle="modal" data-target="#modify1">
                    <a style="color: white;font-weight: bolder;width: 60px;height: 23px;"
                       class="btn btn-primary btn-xs" onclick="update(this)">编辑</a>
                </div>
            </td>
        </tr>
        <%-- <tr>
             <td><img src="" class="changes-img"></td>
             <td class="changes-username"></td>
             <td class="changes-departmentname"></td>
             <td class="changes-positionname"><span class="font_bk"></span></td>
             <td class="changes-time"></td>
             <td>
                 <a href="查看"></a>
                 <a href="#"></a>
             </td>
         </tr>--%>
        </tbody>
    </table>
    <div id="div4" style="position: absolute;top:680px; left:1300px;">
        <input type="button" value="首页" onclick="sun()">
        <input type="button" value="上一页" onclick="subtract()">
        <input type="button" value="下一页" onclick="add()">
    </div>
</div>


<div class="modal fade" id="modify1" role="dialog" aria-labelledby="modifyModalLabel" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="modifyModalLabel">修改员工职位变动信息</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <form class="form-horizontal" action="">
                        <%--   <div class="form-group ">
                               <label for="select1" class="col-xs-3 control-label">原职位：</label>
                               <div class="col-xs-8 ">
                                   <select name="select1" id="select1" class="select4">
                                       <option value="1">设计师</option>
                                       <option value="2">技术经理</option>
                                       <option value="3">经理</option>
                                       <option value="4">主管</option>
                                   </select>
                               </div>
                           </div>--%>
                        <div class="form-group">
                            <label for="select2" class="col-xs-3 control-label">变动类型：</label>
                            <div class="col-xs-8">
                                <select name="select2" id="select2" class="select4">
                                    <option value="转正">转正</option>
                                    <option value="平调">平调</option>
                                    <option value="升迁">升迁</option>
                                    <option value="退休">退休</option>
                                    <option value="离职">离职</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select3" class="col-xs-3 control-label">变动后职位：</label>
                            <div class="col-xs-8">
                                <select name="select3" id="select3" class="select4">
                                    <option value="设计师">设计师</option>
                                    <option value="技术经理">技术经理</option>
                                    <option value="经理">经理</option>
                                    <option value="主管">主管</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="select4" class="col-xs-3 control-label">变动后部门：</label>
                            <div class="col-xs-8">
                                <select name="select4" id="select4" class="select4">
                                    <option value="设计部">设计部</option>
                                    <option value="工程部">工程部</option>
                                    <option value="市场部">市场部</option>
                                    <option value="财务部">财务部</option>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
                <button type="button" class="btn btn-xs btn-green" onclick="updateChange()" data-dismiss="modal"
                        aria-label="Close">保 存</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<%--<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade in active" id="home">
        <table class="table_01">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>部门</td>
                <td>职称</td>
                <td>日期</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody class="tbody">
            <tr class="tr1">
                <td><img src="" class="changes-img"></td>
                <td class="changes-username">徐美图</td>
                <td class="changes-departmentname">部门</td>
                <td class="changes-positionname"><span class="font_bk">职称</span></td>
                <td class="changes-time">09/07/2016-11/07/2016</td>
                <td>
                    <a href="查看"></a>
                    <a href="#">编辑</a>
                </td>
            </tr>
            <tr class="tr1">
                <td><img src="" class="changes-img"></td>
                <td class="changes-username">徐美图</td>
                <td class="changes-departmentname">市场部</td>
                <td class="changes-positionname"><span class="font_bk">调休</span></td>
                <td class="changes-time">09/07/2016-11/07/2016</td>
                <td>
                    <a href="查看"></a>
                    <a href="#">编辑</a>
                </td>
            </tr>
            <tr class="tr1">
                <td><img src="" class="changes-img"></td>
                <td class="changes-username">徐美图</td>
                <td class="changes-departmentname">市场部</td>
                <td class="changes-positionname"><span class="font_bk">调休</span></td>
                <td class="changes-time">09/07/2016-11/07/2016</td>
                <td>
                    <a href="查看"></a>
                    <a href="#">编辑</a>
                </td>
            </tr>
            &lt;%&ndash; <tr>
                 <td><img src="" class="changes-img"></td>
                 <td class="changes-username"></td>
                 <td class="changes-departmentname"></td>
                 <td class="changes-positionname"><span class="font_bk"></span></td>
                 <td class="changes-time"></td>
                 <td>
                     <a href="查看"></a>
                     <a href="#"></a>
                 </td>
             </tr>&ndash;%&gt;
            </tbody>
        </table>
    </div>
</div>--%>

</body>
</html>
