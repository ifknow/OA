<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="utf-8">
    <title>员工考勤</title>
    <link rel="stylesheet" type="text/css" href="css/test.css"/>
    <link rel="stylesheet"
          href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/leavetest.css"/>
    <link rel="stylesheet" type="text/css" href="css/leavetest01.css"/>
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script
            src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style type="text/css">
        .attend {
            line-height: 100%;
            width: 15%;
            height: 20px;
            padding: 5px;
            overflow: visible;
            float: left;
            margin-top: 10px;
        }

        .progress {
            width: 75%;
            height: 20px;
            background: #615f5f;
            padding: 5px;
            overflow: visible;
            border-radius: 15px;
            border-top: 1px solid #000;
            border-bottom: 1px solid #7992a8;
            float: left;
            margin-top: 10px;

        }

        .progress .progress-bar {
            border-radius: 15px;
            position: relative;
            animation: animate-positive 2s;
        }

        .progress .progress-value {
            display: block;
            padding: 3px 7px;
            font-size: 13px;
            color: #fff;
            border-radius: 4px;
            background: #191919;
            border: 1px solid #000;
            position: absolute;
            top: -40px;
            right: -10px;
        }

        .progress .progress-value:after {
            content: "";
            /* border-top: 10px solid #191919; */
            border-left: 10px solid transparent;
            border-right: 10px solid transparent;
            position: absolute;
            bottom: -6px;
        }

        .progress-bar.active {
            animation: reverse progress-bar-stripes 0.40s linear infinite,
            animate-positive 2s;
        }

        .col-md-offset-3 {
            width: 95%;
            height: 204px;
            margin-left: 20px;
            text-align: center;
        }

        .progress-bar-danger {
            background-color: #4fd9c2
        }

        #users-info {
            margin-top: 10px;
            margin-left: 2%;
            margin-bottom: 5px;
        }
    </style>


    <script>
        $(document).ready(function () {


            getAttendanceList("请假");
            getAttendanceList("迟到/早退")
            getAttendanceList("加班");
            getAttendanceList("出差");
            getAttendanceList("旷工");


        })


        function getAttendanceList(e) {
            var aaa;
            if (e == "请假") {
                aaa = "请假";
            } else if (e == "迟到/早退") {
                aaa = "迟到/早退";
            } else if (e == "加班") {
                aaa = "加班";
            } else if (e == "出差") {
                aaa = "出差";
            } else if (e == "旷工") {
                aaa = "旷工";
            } else {
                aaa = e.innerText;
            }

            $.ajax({
                url: "showAttendanceServlet",
                type: "post",
                async: "true",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    leaveclass: aaa
                },
                success: function (date) {

                    var str;
                    var count;
                    console.log(date)

                    for (var i = 0; i < date[0].length; i++) {
                        str += "<tr id='" + date[0][i].attendanceId + "'>" +
                            "<td><img style='width:64px;height:64px;' src=" + date[0][i].headimg + " ></td>" +
                            "<td>" + date[0][i].username + "</td>" +
                            "<td>" + date[0][i].departmentName + "</td>" +
                            "<td><span class='font_bk1' >" + date[0][i].leaveType2 + "</span></td>" +
                            "<td>" + date[0][i].startTime + "-" + date[0][i].endTime + "</td>" +
                            "<td>" + date[0][i].aduitState + "</td>" +
                            "<td>" +
                            "<a style='text-decoration: none;cursor: pointer;font-weight: bolder;' onclick=" + '"' + "loadPage('verifyAttendanceLate.jsp?attendanceId=" + date[0][i].attendanceId + "')" + '"' + ">审核&nbsp;&nbsp;&nbsp;</a>" +
                            "<a style='text-decoration: none;cursor: pointer;font-weight: bolder; color: red;' onclick='deleteAttendanceLate(this)'>删除</a>" +
                            "</td>" +
                            "</tr>";

                    }
                    if (aaa == "请假") {
                        $("#attendance_leave_body1").html(str)

                        $("#depart1_1").html(eval((date[3][0].count / date[2]) * 100) + "%")
                        $("#depart1_2").html(eval((date[3][1].count / date[2]) * 100) + "%")
                        $("#depart1_3").html(eval((date[3][2].count / date[2]) * 100) + "%")
                        $("#depart1_4").html(eval((date[3][3].count / date[2]) * 100) + "%")

                        $("#leaveHTML1_1").html(date[1][0].count + "人")
                        $("#leave1_1").width(eval((date[1][0].count / date[2]) * 100) + "%")
                        $("#leaveHTML1_2").html(date[1][1].count + "人")
                        $("#leave1_2").width(eval((date[1][1].count / date[2]) * 100) + "%")
                        $("#leaveHTML1_3").html(date[1][2].count + "人")
                        $("#leave1_3").width(eval((date[1][2].count / date[2]) * 100) + "%")
                        $("#leaveHTML1_4").html(date[1][3].count + "人")
                        $("#leave1_4").width(eval((date[1][3].count / date[2]) * 100) + "%")


                    }
                    if (aaa == "迟到/早退") {
                        $("#attendance_leave_body2").html(str)

                        $("#leaveHTML2_1").html(date[1][0].count + "人")
                        $("#leave2_1").width(eval((date[1][0].count / date[2]) * 100) + "%")
                        $("#leaveHTML2_2").html(date[1][1].count + "人")
                        $("#leave2_2").width(eval((date[1][1].count / date[2]) * 100) + "%")
                        // $("#leaveHTML2_3").html(date[1][2].count+"人")
                        // $("#leave2_3").width(eval((date[1][2].count/date[2])*100)+"%")
                        // $("#leaveHTML2_4").html(date[1][3].count+"人")
                        // $("#leave2_4").width(eval((date[1][3].count/date[2])*100)+"%")

                        $("#depart2_1").html(eval((date[3][0].count / date[2]) * 100) + "%")
                        $("#depart2_2").html(eval((date[3][1].count / date[2]) * 100) + "%")
                        $("#depart2_3").html(eval((date[3][2].count / date[2]) * 100) + "%")
                        $("#depart2_4").html(eval((date[3][3].count / date[2]) * 100) + "%")


                    }
                    if (aaa == "加班") {
                        $("#attendance_leave_body3").html(str)

                        $("#leaveHTML3_1").html(date[1][0].count + "人")
                        $("#leave3_1").width(eval((date[1][0].count / date[2]) * 100) + "%")
                        // $("#leaveHTML3_2").html(date[1][1].count+"人")
                        // $("#leave3_2").width(eval((date[1][1].count/date[2])*100)+"%")
                        // $("#leaveHTML3_3").html(date[1][2].count+"人")
                        // $("#leave3_3").width(eval((date[1][2].count/date[2])*100)+"%")
                        // $("#leaveHTML3_4").html(date[1][3].count+"人")
                        // $("#leave3_4").width(eval((date[1][3].count/date[2])*100)+"%")

                        $("#depart3_1").html(eval((date[3][0].count / date[2]) * 100) + "%")
                        $("#depart3_2").html(eval((date[3][1].count / date[2]) * 100) + "%")
                        $("#depart3_3").html(eval((date[3][2].count / date[2]) * 100) + "%")
                        $("#depart3_4").html(eval((date[3][3].count / date[2]) * 100) + "%")


                    }
                    if (aaa == "出差") {
                        $("#attendance_leave_body4").html(str)

                        $("#leaveHTML4_1").html(date[1][0].count + "人")
                        $("#leave4_1").width(eval((date[1][0].count / date[2]) * 100) + "%")
                        // $("#leaveHTML4_2").html(date[1][1].count+"人")
                        // $("#leave4_2").width(eval((date[1][1].count/date[2])*100)+"%")
                        // $("#leaveHTML4_3").html(date[1][2].count+"人")
                        // $("#leave4_3").width(eval((date[1][2].count/date[2])*100)+"%")
                        // $("#leaveHTML4_4").html(date[1][3].count+"人")
                        // $("#leave4_4").width(eval((date[1][3].count/date[2])*100)+"%")

                        $("#depart4_1").html(eval((date[3][0].count / date[2]) * 100) + "%")
                        $("#depart4_2").html(eval((date[3][1].count / date[2]) * 100) + "%")
                        $("#depart4_3").html(eval((date[3][2].count / date[2]) * 100) + "%")
                        $("#depart4_4").html(eval((date[3][3].count / date[2]) * 100) + "%")


                    }
                    if (aaa == "旷工") {
                        $("#attendance_leave_body5").html(str)

                        $("#leaveHTML5_1").html(date[1][0].count + "人")
                        $("#leave5_1").width(eval((date[1][0].count / date[2]) * 100) + "%")
                        // $("#leaveHTML5_2").html(date[1][1].count+"人")
                        // $("#leave5_2").width(eval((date[1][1].count/date[2])*100)+"%")
                        // $("#leaveHTML5_3").html(date[1][2].count+"人")
                        // $("#leave5_3").width(eval((date[1][2].count/date[2])*100)+"%")
                        // $("#leaveHTML5_4").html(date[1][3].count+"人")
                        // $("#leave5_4").width(eval((date[1][3].count/date[2])*100)+"%")

                        $("#depart5_1").html(eval((date[3][0].count / date[2]) * 100) + "%")
                        $("#depart5_2").html(eval((date[3][1].count / date[2]) * 100) + "%")
                        $("#depart5_3").html(eval((date[3][2].count / date[2]) * 100) + "%")
                        $("#depart5_4").html(eval((date[3][3].count / date[2]) * 100) + "%")


                    }

                    //for (var i = 0; i < date[1].length; i++) {
                    //console.log(date[1][i].leaveType2)
                    //	console.log(date[1][i].count)
                    //	console.log("-------------")
                    //	}

                }

            })
        }

        //删除用户迟到信息
        function deleteAttendanceLate(value) {
            question = confirm("确实要刪除吗?")

            if (question != "0") {
                $.ajax({
                    url: "delAttendanceServlet",
                    type: "post",
                    async: "true",
                    dataType: "json",
                    contentType: "application/x-www-form-urlencoded;charset=utf-8",
                    data: {
                        attendanceId: value.parentNode.parentNode.id

                    },
                    success: function (date) {
                        var str
                        for (var i = 0; i < date.length; i++) {
                            str = date[i]
                        }
                        question = confirm(str)
                        if (question != "0") {

                            getAttendanceList("请假");
                            getAttendanceList("迟到/早退")
                            getAttendanceList("加班");
                            getAttendanceList("出差");
                            getAttendanceList("旷工");

                        } else {
                            getAttendanceList("请假");
                            getAttendanceList("迟到/早退")
                            getAttendanceList("加班");
                            getAttendanceList("出差");
                            getAttendanceList("旷工");
                        }
                    }
                })

            }
        }

        //根据条件查询用户
        function query() {
            $.ajax({
                url: "SearchAttendanceLeave",
                type: "post",
                async: "true",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    username: $("#username").val(),
                    aduitState: $("#aduitState").val(),
                    startTime: $("#startTime").val(),
                    endTime: $("#endTime").val(),
                },
                success: function (date) {
                    var str;
                    console.log(date)
                    for (var i = 0; i < date.length; i++) {
                        str += "<tr id='" + date[i].attendanceId + "'>" +
                            "<td><img style='width:64px;height:64px;' src=" + date[i].headimg + "></td>" +
                            "<td>" + date[i].username + "</td>" +
                            "<td>" + date[i].departmentName + "</td>" +
                            "<td><span class='font_bk1' >" + date[i].leaveType2 + "</span></td>" +
                            "<td>" + date[i].startTime + "-" + date[i].endTime + "</td>" +
                            "<td>" + date[i].aduitState + "</td>" +
                            "<td>" +
                            "<a style='text-decoration: none;cursor: pointer;font-weight: bolder;' onclick=" + '"' + "loadPage('verifyAttendanceLate.jsp?attendanceId=" + date[i].attendanceId + "')" + '"' + ">审核&nbsp;&nbsp;&nbsp;</a>" +
                            "<a style='text-decoration: none;cursor: pointer;font-weight: bolder; color: red;' onclick='deleteAttendanceLate(this)'>删除</a>" +
                            "</td>" +
                            "</tr>";

                    }
                    $("#attendance_leave_body1").html(str)
                    $("#attendance_leave_body2").html(str)
                    $("#attendance_leave_body3").html(str)
                    $("#attendance_leave_body4").html(str)
                    $("#attendance_leave_body5").html(str)
                }
            })
        }

        //清空条件
        function reset() {
            $("#username").val("")
            //$('#aduitState').empty();
            $('#aduitState').val("")
            $("#startTime").val("")
            $("#endTime").val("")
        }

        //添加员工信息
        function addAttendance() {
            $.ajax({
                url: "addAttendanceServlet",
                type: "post",
                async: "true",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    userId: $("#a_userId").val(),
                    username: $("#a_username").val(),
                    leaveclass: $("#a_leaveType1").val(),
                    leavetype: $("#a_leaveType2").val(),
                    startTime: $("#a_startTime").val(),
                    endTime: $("#a_endTime").val(),
                    aduitState: $("#a_aduitState").val()
                },
                success: function (date) {
                    var str
                    for (var i = 0; i < date.length; i++) {
                        str = date[i]
                    }
                    question = confirm(str)
                    if (question != "0") {
                        getAttendanceList();
                    }
                }
            })
        }
    </script>
</head>
<body>
<div role="tabpanel" class="tab-pane active" id="users-info">
    <div class="data-div">
        <div class="row" style="margin-bottom: 5px;">
            <div class="col-xs-3" style="width: 300px;">
                <input type="text" class="form-control" id="username"
                       placeholder="请输入员工名称">
            </div>
            <div class="col-xs-3" style="width: 300px;">
                <select class="form-control select2" id="aduitState"
                        data-placeholder="请选审核状态" style="width: 100%;">
                    <option selected="selected" value=""></option>
                    <option value="未审核">未审核</option>
                    <option value="审核通过">审核通过</option>
                    <option value="审核未通过">审核未通过</option>
                </select>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right lte-date" data-toggle="datepicker"
                           id="startTime" name="startTime" value="" placeholder="开始时间">
                </div>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control pull-right lte-date" data-toggle="datepicker"
                           id="endTime" name="endTime" value="" placeholder="结束时间">
                </div>
            </div>
            <div class="col-xs-2" style="width: 212px;float: right;padding-left: 15px;">
                <button id="queryBtn" class="btn btn-primary my-btn" onclick="query()">
                    <i class="fa fa-search button-in-i"></i>查询
                </button>
                <button id="clearBtn" class="btn btn-danger my-btn" onclick="reset()">
                    <i class="fa fa-trash button-in-i"></i>清空
                </button>
                <button id="add1" class="btn btn-primary my-btn" value="添加" data-toggle="modal"
                        data-target="#addUserAttendance" title="添加员工考勤信息">
                    <i class="fa fa-cart-arrow-down button-in-i"></i>添加
                </button>

            </div>
        </div>
    </div>
</div>

<div class="herdertop">
    <ul id="myTab" class="nav nav-tabs">
        <li class="active"><a href="#home" data-toggle="tab" onclick="getAttendanceList(this)">请假</a></li>
        <li><a href="#late" data-toggle="tab" onclick="getAttendanceList(this)">迟到/早退</a></li>
        <li><a href="#text1" data-toggle="tab" onclick="getAttendanceList(this)">加班</a></li>
        <li><a href="#text2" data-toggle="tab" onclick="getAttendanceList(this)">出差</a></li>
        <li><a href="#text3" data-toggle="tab" onclick="getAttendanceList(this)">旷工</a></li>
    </ul>
</div>
<div class="costop">
    <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle"
                data-toggle="dropdown" style="border: none">
            最近一个月 <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu"
            style="min-width: 100%; background-color: #0de6a1">
            <li><a href="#">最近15天</a></li>
            <li class="divider"></li>
            <li><a href="#">最近30天</a></li>
        </ul>
    </div>
</div>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade in active" id="home">
        <table class="table_01" id="attendance_leave">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">财务部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            请假类型 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">调休</a></li>
                            <li class="divider"></li>
                            <li><a href="#">事假</a></li>
                            <li class="divider"></li>
                            <li><a href="#">产假</a></li>
                            <li class="divider"></li>
                            <li><a href="#">病假</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>

            <tbody id="attendance_leave_body1">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>

        <div class="cols01" id="aa">
            <div class="col-md-offset-3 col-md-6">

                <div style="height: 50px;">
                    <div class="n_left">调休</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-info progress-bar-striped active"
                                style="width: 0%;" id="leave1_1">
                        </div>
                    </div>
                    <div class="p_left" id="leaveHTML1_1">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">病假</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-success progress-bar-striped active"
                                style="width: 0%;" id="leave1_2">
                        </div>
                    </div>
                    <div class="p_left" id="leaveHTML1_2">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">产假</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-warning progress-bar-striped active"
                                style="width: 0%;" id="leave1_3">
                        </div>
                    </div>
                    <div class="p_left" id="leaveHTML1_3">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">事假</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-danger progress-bar-striped active"
                                style="width: 0%;" id="leave1_4">
                        </div>
                    </div>
                    <div class="p_left" id="leaveHTML1_4">0人</div>
                </div>

            </div>
        </div>
        <div class="cols02">
            <div id="col">各部门请假比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p id="depart1_1">0%</p> <span style="font-weight: bolder;color: white;">市场部</span>
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart1_2">0%</p> <span style="font-weight: bolder;color: white;">设计部</span>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart1_3">0%</p> <span style="font-weight: bolder;color: white;">工程部</span>
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p id="depart1_4">0%</p> <span style="font-weight: bolder;color: white;">财务部</span>
                    </td>
                </tr>
            </table>

        </div>
        <ul class="pagination  pagination-sm" id="footer" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>

    </div>
    <div class="tab-pane fade" id="late">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">财务部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            出勤 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">迟到</a></li>
                            <li class="divider"></li>
                            <li><a href="#">早退</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="attendance_leave_body2">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>


        <div class="cols01" id="bb">
            <div class="col-md-offset-3 col-md-6">
                <div style="height: 50px;">
                    <div class="n_left">迟到</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-info progress-bar-striped active"
                                style="width: 0%;" id="leave2_1"></div>
                    </div>
                    <div class="p_left" id="leaveHTML2_1">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">早退</div>
                    <div class="progress">

                        <div
                                class="progress-bar progress-bar-success progress-bar-striped active"
                                style="width: 0%;" id="leave2_2"></div>
                    </div>
                    <div class="p_left" id="leaveHTML2_2">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-warning progress-bar-striped active"
                                style="width: 0%;" id="leave2_3"></div>
                    </div>
                    <div class="p_left" id="leaveHTML2_3">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-danger progress-bar-striped active"
                                style="width: 0%;" id="leave2_4"></div>

                    </div>
                    <div class="p_left" id="leaveHTML2_4">0人</div>
                </div>
            </div>
        </div>

        <div class="cols02">
            <div id="col1">各部门早退/迟到比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p id="depart2_1">0%</p> <span style="font-weight: bolder;color: white;">市场部</span>
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart2_2">0%</p> <span style="font-weight: bolder;color: white;">设计部</span>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart2_3">0%</p> <span style="font-weight: bolder;color: white;">工程部</span>
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p id="depart2_4">0%</p> <span style="font-weight: bolder;color: white;">财务部</span>
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer1" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>

    </div>
    <div class="tab-pane fade" id="text1">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">财务部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            加班 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">加班</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="attendance_leave_body3">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>


        <div class="cols01" id="cc">
            <div class="col-md-offset-3 col-md-6">

                <div style="height: 50px;">
                    <div class="n_left">加班</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-info progress-bar-striped active"
                                style="width: 0%;" id="leave3_1"></div>
                    </div>
                    <div class="p_left" id="leaveHTML3_1">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">

                        <div
                                class="progress-bar progress-bar-success progress-bar-striped active"
                                style="width: 0%;" id="leave3_2"></div>
                    </div>
                    <div class="p_left" id="leaveHTML3_2">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-warning progress-bar-striped active"
                                style="width: 0%;" id="leave3_3"></div>
                    </div>
                    <div class="p_left" id="leaveHTML3_3">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-danger progress-bar-striped active"
                                style="width: 0%;" id="leave3_4"></div>

                    </div>
                    <div class="p_left" id="leaveHTML3_4">0人</div>
                </div>
            </div>
        </div>

        <div class="cols02">
            <div id="col2">各部门加班比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p id="depart3_1">0%</p> <span style="font-weight: bolder;color: white;">市场部</span>
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart3_2">0%</p> <span style="font-weight: bolder;color: white;">设计部</span>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart3_3">0%</p> <span style="font-weight: bolder;color: white;">工程部</span>
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p id="depart3_4">0%</p><span style="font-weight: bolder;color: white;">财务部</span>
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer2" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
    <div class="tab-pane fade" id="text2">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">财务部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            出差 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">出差</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="attendance_leave_body4">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>


        <div class="cols01" id="dd">
            <div class="col-md-offset-3 col-md-6">
                <div style="height: 50px;">
                    <div class="n_left">出差</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-info progress-bar-striped active"
                                style="width: 0%;" id="leave4_1"></div>
                    </div>
                    <div class="p_left" id="leaveHTML4_1">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">

                        <div
                                class="progress-bar progress-bar-success progress-bar-striped active"
                                style="width: 0%;" id="leave4_2"></div>
                    </div>
                    <div class="p_left" id="leaveHTML4_2">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-warning progress-bar-striped active"
                                style="width: 0%;" id="leave4_3"></div>
                    </div>
                    <div class="p_left" id="leaveHTML4_3">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-danger progress-bar-striped active"
                                style="width: 0%;" id="leave4_4"></div>

                    </div>
                    <div class="p_left" id="leaveHTML4_4">0人</div>
                </div>

            </div>
        </div>

        <div class="cols02">
            <div id="col3">各部门出差比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p id="depart4_1">0%</p> <span style="font-weight: bolder;color: white;">市场部</span>
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart4_2">0%</p> <span style="font-weight: bolder;color: white;">设计部</span>
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart4_3">0%</p> <span style="font-weight: bolder;color: white;">工程部</span>
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p id="depart4_4">0%</p><span style="font-weight: bolder;color: white;">财务部</span>
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer3" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
    <div class="tab-pane fade" id="text3">
        <table class="table_02">
            <thead>
            <tr>
                <td>相片</td>
                <td>姓名</td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            部门 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">市场部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">设计部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">工程部</a></li>
                            <li class="divider"></li>
                            <li><a href="#">财务部</a></li>
                        </ul>
                    </div>
                </td>
                <td>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle"
                                data-toggle="dropdown" style="border: none">
                            旷工 <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu" style="min-width: 100%">
                            <li><a href="#">旷工</a></li>
                        </ul>
                    </div>
                </td>
                <td>日期</td>
                <td>审核状态</td>
                <td>操作</td>
            </tr>
            </thead>
            <tbody id="attendance_leave_body5">
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>


        <div class="cols01" id="ee">
            <div class="col-md-offset-3 col-md-6">
                <div style="height: 50px;">
                    <div class="n_left">旷工</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-info progress-bar-striped active"
                                style="width: 0%;" id="leave5_1"></div>
                    </div>
                    <div class="p_left" id="leaveHTML5_1">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">

                        <div
                                class="progress-bar progress-bar-success progress-bar-striped active"
                                style="width: 0%;" id="leave5_2"></div>
                    </div>
                    <div class="p_left" id="leaveHTML5_2">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-warning progress-bar-striped active"
                                style="width: 0%;" id="leave5_3"></div>
                    </div>
                    <div class="p_left" id="leaveHTML5_3">0人</div>
                </div>
                <div style="height: 50px;">
                    <div class="n_left">暂无</div>
                    <div class="progress">
                        <div
                                class="progress-bar progress-bar-danger progress-bar-striped active"
                                style="width: 0%;" id="leave5_4"></div>

                    </div>
                    <div class="p_left" id="leaveHTML5_4">0人</div>
                </div>
            </div>
        </div>

        <div class="cols02">
            <div id="col4">各部门旷工比例</div>
            <table>
                <tr>
                    <td style="background-color: cornflowerblue">
                        <p id="depart5_1">0%</p> 市场部
                    </td>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart5_2">0%</p> 设计部
                    </td>
                </tr>
                <tr>
                    <td style="background-color: rgb(233, 108, 5)">
                        <p id="depart5_3">0%</p> 工程部
                    </td>
                    <td style="background-color: cornflowerblue">
                        <p id="depart5_4">0%</p> 财务部
                    </td>
                </tr>
            </table>
        </div>

        <ul class="pagination  pagination-sm" id="footer4" style="margin-left: 2%">
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">...</a></li>
            <li><a href="#">尾页</a></li>
        </ul>
    </div>
</div>
<!--弹出添加用户窗口-->
<div class="modal fade" id="addUserAttendance" role="dialog" aria-labelledby="gridSystemModalLabel" style="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridSystemModalLabel">添加员工考勤信息</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group ">
                        <label for="a_username" class="col-xs-3 control-label">员工ID：</label>
                        <div class="col-xs-8 ">
                            <input type="text" class="form-control input-sm duiqi" id="a_userId" placeholder="">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="a_username" class="col-xs-3 control-label">员工姓名：</label>
                        <div class="col-xs-8 ">
                            <input type="text" class="form-control input-sm duiqi" id="a_username" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="select2-departmentId" class="col-xs-3 control-label">部门：</label>
                        <div class="col-xs-8">
                            <select id="select2-departmentId"
                                    class=" form-control select-duiqi"> <%-- onfocus="update2DepartmentId()" --%>
                                <option>市场部</option>
                                <option>工程部</option>
                                <option>设计部</option>
                                <option>财务部</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="a_leaveType1" class="col-xs-3 control-label">请假类型1：</label>
                        <div class="col-xs-8 ">
                            <!--<input type="" class="form-control input-sm duiqi" id="sLink" placeholder="">-->
                            <select id="a_leaveType1" name="a_leaveType1" class=" form-control select-duiqi">
                                <option value="">考勤分类-</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="a_leaveType2" class="col-xs-3 control-label">请假类型2：</label>
                        <div class="col-xs-8 ">
                            <!--<input type="" class="form-control input-sm duiqi" id="sLink" placeholder="">-->
                            <select id="a_leaveType2" name="a_leaveType2" class=" form-control select-duiqi">
                                <option value="">考勤分类2</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="startTime" class="col-xs-3 control-label">开始时间：</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control pull-right lte-date" data-toggle="datepicker"
                                   id="a_startTime" name="a_startTime" value="" placeholder="开始时间">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="a_endTime" class="col-xs-3 control-label">结束时间：</label>
                        <div class="col-xs-8">
                            <input type="text" class="form-control pull-right lte-date" data-toggle="datepicker"
                                   id="a_endTime" name="a_endTime" value="" placeholder="结束时间">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="a_aduitState" class="col-xs-3 control-label">审核状态：</label>
                        <div class="col-xs-8 ">
                            <!--<input type="" class="form-control input-sm duiqi" id="sLink" placeholder="">-->
                            <select id="a_aduitState" name="a_aduitState" class=" form-control select-duiqi">
                                <option value="未审核">未审核</option>
                                <option value="审核通过">审核通过</option>
                                <option value="审核不通过">审核不通过</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
                <button type="button" class="btn btn-xs btn-green" onclick="addAttendance()" aria-label="Close">保 存
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<script type="text/javascript">
    var leaveType1 = document.getElementById("a_leaveType1");
    var leaveType2 = document.getElementById("a_leaveType2");
    var all = [
        {
            name: "请假",
            leaveType2: ['调休', '事假', '病假', '产假']
        },
        {
            name: "迟到/早退",
            leaveType2: ['迟到', '早退']
        },
        {
            name: "出差",
            leaveType2: ['出差']
        },
        {
            name: "加班",
            leaveType2: ['加班']
        },
        {
            name: "旷工",
            leaveType2: ['旷工']
        },
    ]
    for (var i = 0; i < all.length; i++) {
        var op = document.createElement("option");
        op.innerText = all[i].name;
        op.value = i;
        leaveType1.appendChild(op);

        leaveType1.onchange = function () {
            leaveType2.innerHTML = "<option value ='考勤类型2'>考勤类型2</option>";
            for (var j = 0; j < all[leaveType1.value].leaveType2.length; j++) {
                var op = document.createElement("option");
                op.innerText = all[leaveType1.value].leaveType2[j];
                op.value = j;
                leaveType2.appendChild(op);


            }
        }
    }

</script>

<script>


    // function update2DepartmentId(){
    //     $.ajax({
    //         url: "/refineSystem/QueryDepartmentIdServlet",
    //         type: "post",
    //         data: {}, //data表示发送的数据
    //         dataType: "json", //定义回调响应的数据格式为JSON字符串,该属性可以省略
    //         success: function (data) { //成功响应的结果
    //             if (data != null) {
    //                 $("#select2-departmentId").empty();
    //                 for (var i = 0; i < data.length; i++) {
    //                     $("#select2-departmentId").append("<option value='" + data[i]["departmentId"] + "'>" + data[i]["departmentName"] + "</option>");
    //                 }
    //             }
    //         },
    //     })
    // }

    $(function () {
        $('#myTab li:eq(1) a').tab('show');
    });

    $(function () {
        // Date picker 时间插件
        $('[data-toggle="datepicker"]').datepicker({
            language: 'zh-CN',
            minView: "month",//设置只显示到月份
            format: 'yyyy-mm-dd',
            autoclose: true,
            autoHide: true,
            zIndex: 2048,
        });

    });

    $(function () {
        // Date picker 时间插件
        $('#d4311').datetimepicker({
            language: 'zh-CN',
            autoclose: true,
            minView: "month",//设置只显示到月份
            format: 'yyyy-mm-dd'
        });

        $('#d4312').datetimepicker({
            language: 'zh-CN',
            autoclose: true,
            minView: "month",//设置只显示到月份
            format: 'yyyy-mm-dd'
        });

        $(".select2").select2({
            allowClear: true
        });
    });

</script>

</body>
</html>
