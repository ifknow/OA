<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta charset="UTF-8">
    <title>审核用户迟到</title>
    <!-- 小图标 -->
    <link rel="stylesheet" type="text/css"
          href="css/fontawesome/css/font-awesome.min.css">
    <!-- date -->
    <link rel="stylesheet" type="text/css"
          href="css/datepicker/datepicker3.css">
    <!-- Select2 -->
    <link rel="stylesheet" type="text/css"
          href="css/select2/select2.min.css">
    <script src="https://cdn.bootcss.com/jquery/2.1.1/jquery.min.js"></script>
    <script
            src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>

        #title {
            width: 100%;
            height: 50px;
            background-color: rgb(83, 149, 223);
            color: white;
            font-weight: bolder;
            font-size: 21px;
            margin: 0px auto;
            text-align: center;
            padding-top: 10px;
        }

        #body {
			height: 600px;
            background-color: white;
            margin: 0px auto;
			padding-top: 20px;
        }

        #table1 td {
            height: 30px;
            font-size: 15px;
        }

        #table1 {
            margin: 0 auto;
            padding-top: 40px;
        }

    </style>
    <%
        String id = request.getParameter("attendanceId");
        System.out.println(id);
    %>
    <script type="text/javascript">
        //页面加载完成后显示查询信息
        $(function () {
            console.log("123")
            windowsonload();
            console.log("456")
        })

        //查询信息
        function windowsonload() {
            $.ajax({
                url: "verifyAttendanceLate",
                type: "post",
                async: "true",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    attendanceId:<%=request.getParameter("attendanceId")%>
                },
                success: function (date) {
                    console.log(date)
                    for (var i = 0; i < date.length; i++) {
                        $("#username").val(date[i].username);
                        $("#departmentName").val(date[i].departmentName);
                        $("#leaveType2").val(date[i].leaveType2);
                        $("#startTime").val(date[i].startTime);
                        $("#endTime").val(date[i].endTime);
                        $("#aduitState").val(date[i].aduitState);
                    }

                }
            })
        }

        //修改信息
        function verifyAttendanceLate() {
            $.ajax({
                url: "updateAtt",
                type: "post",
                async: "true",
                dataType: "json",
                contentType: "application/x-www-form-urlencoded;charset=utf-8",
                data: {
                    attendanceId:<%=request.getParameter("attendanceId")%>,
                    username: $("#username").val(),
                    departmentName: $("#departmentName").val(),
                    leaveType2: $("#leaveType2").val(),
                    startTime: $("#startTime").val(),
                    endTime: $("#endTime").val(),
                    aduitState: $("#aduitState").val()
                },
                success: function (date) {
                    var str
                    for (var i = 0; i < date.length; i++) {
                        str = date[i]
                    }
                    question = confirm(str)
                    if (question != "0") {
                        windowsonload();
                    }
                }

            })
        }
    </script>

</head>
<body>
<div>
    <div id="title">
        <span id="name">审核用户</span>
    </div>
    <div id="body">
        <table id="table1">
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">姓名:</td>
                <td><input class="form-control" disabled unselectable="on" type="text" id="username" value="" class="form-control"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">部门:</td>
                <td><input class="form-control" disabled unselectable="on" type="text" id="departmentName" value="" class="form-control">
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">请假类型:</td>
                <td><input class="form-control" disabled type="text" id="leaveType2" value="" class="form-control"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">开始日期:</td>
                <td><input class="form-control" disabled type="text" id="startTime" value="" class="form-control"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">结束日期:</td>
                <td><input class="form-control" disabled type="text" id="endTime" value="" class="form-control"></td>
            </tr>
            <tr>
                <td></td>
            </tr>
            <tr>
                <td style="width: 100px;">审核状态:</td>
                <td>
                    <select id="aduitState" value="" class="form-control">
                        <option value=""></option>
                        <option value="未审核">未审核</option>
                        <option value="审核通过">审核通过</option>
                        <option value="审核未通过">审核未通过</option>
                    </select>
                </td>
            </tr>
        </table>
		<div class="form-group" style="text-align: center; padding-top: 50px;">
			<input class="btn btn-primary" type="button" value="保存" onclick="verifyAttendanceLate()"/>
			<input class="btn btn-default" type="button" value="返回" onclick="loadPage('attendance.jsp')"/>
		</div>
    </div>

</div>
</body>
</html>