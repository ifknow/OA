/*
 Navicat Premium Data Transfer

 Source Server         : Mysql 8.0
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : refine

 Target Server Type    : MySQL
 Target Server Version : 80017
 File Encoding         : 65001

 Date: 31/10/2019 08:56:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for assessor
-- ----------------------------
DROP TABLE IF EXISTS `assessor`;
CREATE TABLE `assessor`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `aduitState` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of assessor
-- ----------------------------
INSERT INTO `assessor` VALUES (1, '未审核');
INSERT INTO `assessor` VALUES (2, '审核通过');
INSERT INTO `assessor` VALUES (3, '审核未通过');

-- ----------------------------
-- Table structure for attendance
-- ----------------------------
DROP TABLE IF EXISTS `attendance`;
CREATE TABLE `attendance`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '考勤id',
  `userid` int(11) NULL DEFAULT NULL COMMENT '员工id',
  `leaveclass` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '考勤分类 1.请假 2.迟到/早退 3.出差 4.加班 5.旷工',
  `leavetype` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '考勤类型 如1.调休 2.病假 3.产假 4.事假',
  `aduitState` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '审核状态 1.未审核 2.已审核',
  `starttime` datetime(0) NULL DEFAULT NULL COMMENT '开始日期',
  `endtime` datetime(0) NULL DEFAULT NULL COMMENT '结束日期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of attendance
-- ----------------------------
INSERT INTO `attendance` VALUES (1, 4, '请假', '调休', '审核通过', '2019-10-01 00:00:00', '2019-10-02 00:00:00');
INSERT INTO `attendance` VALUES (2, 4, '迟到/早退', '迟到', '未审核', '2019-10-23 00:00:00', '2019-10-23 00:00:00');
INSERT INTO `attendance` VALUES (3, 191, '迟到/早退', '早退', '审核通过', '2019-10-29 00:00:00', '2019-10-29 00:00:00');
INSERT INTO `attendance` VALUES (4, 292, '迟到/早退', '迟到', '审核未通过', '2019-10-28 00:00:00', '2019-10-28 00:00:00');
INSERT INTO `attendance` VALUES (5, 5, '迟到/早退', '早退', '未审核', '2019-10-29 17:17:56', '2019-10-29 17:18:00');
INSERT INTO `attendance` VALUES (7, 191, '请假', '病假', '未审核', '2019-10-30 17:20:05', '2019-10-30 17:20:06');
INSERT INTO `attendance` VALUES (8, 292, '请假', '产假', '未审核', '2019-10-30 17:20:32', '2019-10-30 17:20:34');
INSERT INTO `attendance` VALUES (9, 5, '请假', '事假', '未审核', '2019-10-30 17:21:18', '2019-10-30 17:21:20');
INSERT INTO `attendance` VALUES (10, 4, '加班', '加班', '未审核', '2019-10-30 17:24:16', '2019-10-30 17:24:18');
INSERT INTO `attendance` VALUES (12, 4, '出差', '出差', '未审核', '2019-10-30 17:25:04', '2019-10-30 17:25:06');
INSERT INTO `attendance` VALUES (14, 4, '旷工', '旷工', '未审核', '2019-10-30 17:26:11', '2019-10-30 17:26:13');
INSERT INTO `attendance` VALUES (16, 258, '加班', '加班', '未审核', '2019-10-16 17:28:12', '2019-10-16 17:28:17');
INSERT INTO `attendance` VALUES (17, 293, '出差', '出差', '未审核', '2019-10-30 17:28:50', '2019-10-31 17:28:52');
INSERT INTO `attendance` VALUES (18, 256, '加班', '加班', '未审核', '2019-10-30 17:31:40', '2019-10-30 17:31:42');
INSERT INTO `attendance` VALUES (19, 256, '出差', '出差', '未审核', '2019-10-09 17:32:00', '2019-10-10 17:32:02');
INSERT INTO `attendance` VALUES (20, 256, '旷工', '旷工', '未审核', '2019-10-30 17:32:23', '2019-10-30 17:32:24');
INSERT INTO `attendance` VALUES (21, 292, '出差', '出差', '未审核', '2019-10-29 17:33:33', '2019-10-31 17:33:37');
INSERT INTO `attendance` VALUES (22, 292, '加班', '加班', '未审核', '2019-10-30 17:33:59', '2019-10-30 17:34:01');
INSERT INTO `attendance` VALUES (23, 292, '旷工', '旷工', '未审核', '2019-10-30 17:34:20', '2019-10-30 17:34:22');
INSERT INTO `attendance` VALUES (24, 5, '出差', '出差', '未审核', '2019-10-30 17:35:37', '2019-10-30 17:35:39');
INSERT INTO `attendance` VALUES (25, 5, '加班', '加班', '未审核', '2019-10-30 17:35:54', '2019-10-30 17:35:55');
INSERT INTO `attendance` VALUES (26, 5, '旷工', '旷工', '未审核', '2019-10-30 17:36:12', '2019-10-30 17:36:13');

-- ----------------------------
-- Table structure for changeinfo
-- ----------------------------
DROP TABLE IF EXISTS `changeinfo`;
CREATE TABLE `changeinfo`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '标识ID',
  `userid` int(10) NULL DEFAULT NULL COMMENT '员工ID',
  `typeid` int(10) NULL DEFAULT NULL COMMENT '变动类型ID 1.转正 2.平调 3.升迁 4.退休 5.离职',
  `sPositionId` int(10) NULL DEFAULT NULL COMMENT '原职位ID',
  `ePositionId` int(10) NULL DEFAULT NULL COMMENT '变动后职位ID',
  `aduitState` int(11) NULL DEFAULT 2,
  `changetime` datetime(0) NULL DEFAULT NULL COMMENT '变动时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of changeinfo
-- ----------------------------
INSERT INTO `changeinfo` VALUES (5, 2, 1, 3, 4, 2, '2019-10-22 00:00:00');
INSERT INTO `changeinfo` VALUES (6, 4, 1, 3, 3, 2, '2019-10-22 00:00:00');
INSERT INTO `changeinfo` VALUES (7, 5, 1, 1, 3, 2, '2019-05-22 00:00:00');
INSERT INTO `changeinfo` VALUES (9, 192, 2, 2, 3, 2, '2019-10-30 09:07:15');
INSERT INTO `changeinfo` VALUES (10, 4, 3, 3, 3, 2, '2019-10-30 16:07:28');
INSERT INTO `changeinfo` VALUES (11, 132, 1, 4, 2, 2, '2019-10-30 16:51:17');
INSERT INTO `changeinfo` VALUES (12, 191, 2, 4, 2, 2, '2019-10-30 16:51:17');
INSERT INTO `changeinfo` VALUES (13, 0, 1, 0, 1, 2, '2019-10-30 23:06:12');

-- ----------------------------
-- Table structure for changetype
-- ----------------------------
DROP TABLE IF EXISTS `changetype`;
CREATE TABLE `changetype`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of changetype
-- ----------------------------
INSERT INTO `changetype` VALUES (1, '转正');
INSERT INTO `changetype` VALUES (2, '平调');
INSERT INTO `changetype` VALUES (3, '升迁');
INSERT INTO `changetype` VALUES (4, '退休');
INSERT INTO `changetype` VALUES (5, '离职');

-- ----------------------------
-- Table structure for dempartment
-- ----------------------------
DROP TABLE IF EXISTS `dempartment`;
CREATE TABLE `dempartment`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '部门ID',
  `dempartmentName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dempartment
-- ----------------------------
INSERT INTO `dempartment` VALUES (1, '设计部');
INSERT INTO `dempartment` VALUES (2, '工程部');
INSERT INTO `dempartment` VALUES (3, '市场部');
INSERT INTO `dempartment` VALUES (4, '财务部');

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '登陆ID',
  `userid` int(11) NULL DEFAULT NULL COMMENT '用户ID',
  `logintime` datetime(0) NULL DEFAULT NULL COMMENT '登陆时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 135 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES (1, 2, '2019-10-23 17:12:53');
INSERT INTO `login` VALUES (2, 2, '2019-10-23 18:36:46');
INSERT INTO `login` VALUES (3, 2, '2019-10-23 22:11:32');
INSERT INTO `login` VALUES (4, 2, '2019-10-24 08:46:15');
INSERT INTO `login` VALUES (5, 2, '2019-10-24 14:21:14');
INSERT INTO `login` VALUES (6, 2, '2019-10-24 15:26:23');
INSERT INTO `login` VALUES (7, 2, '2019-10-24 18:59:20');
INSERT INTO `login` VALUES (8, 14, '2019-10-25 09:21:53');
INSERT INTO `login` VALUES (9, 14, '2019-10-25 10:16:31');
INSERT INTO `login` VALUES (10, 128, '2019-10-25 14:07:17');
INSERT INTO `login` VALUES (11, 128, '2019-10-25 14:13:07');
INSERT INTO `login` VALUES (12, 128, '2019-10-25 15:07:07');
INSERT INTO `login` VALUES (13, 14, '2019-10-25 16:06:49');
INSERT INTO `login` VALUES (14, 14, '2019-10-25 16:09:32');
INSERT INTO `login` VALUES (15, 128, '2019-10-25 20:57:36');
INSERT INTO `login` VALUES (16, 128, '2019-10-25 21:27:38');
INSERT INTO `login` VALUES (17, 128, '2019-10-25 23:06:45');
INSERT INTO `login` VALUES (18, 128, '2019-10-25 23:31:29');
INSERT INTO `login` VALUES (19, 128, '2019-10-25 23:37:11');
INSERT INTO `login` VALUES (20, 128, '2019-10-25 23:44:18');
INSERT INTO `login` VALUES (21, 128, '2019-10-25 23:57:41');
INSERT INTO `login` VALUES (22, 128, '2019-10-26 00:03:35');
INSERT INTO `login` VALUES (23, 128, '2019-10-26 08:37:50');
INSERT INTO `login` VALUES (24, 128, '2019-10-26 08:54:10');
INSERT INTO `login` VALUES (25, 128, '2019-10-26 09:34:33');
INSERT INTO `login` VALUES (26, 128, '2019-10-26 09:46:14');
INSERT INTO `login` VALUES (27, 128, '2019-10-26 10:17:50');
INSERT INTO `login` VALUES (28, 128, '2019-10-26 10:19:09');
INSERT INTO `login` VALUES (29, 128, '2019-10-26 10:26:56');
INSERT INTO `login` VALUES (30, 128, '2019-10-26 10:37:37');
INSERT INTO `login` VALUES (31, 128, '2019-10-26 10:49:03');
INSERT INTO `login` VALUES (32, 128, '2019-10-26 10:53:08');
INSERT INTO `login` VALUES (33, 128, '2019-10-26 10:56:44');
INSERT INTO `login` VALUES (34, 128, '2019-10-26 11:01:01');
INSERT INTO `login` VALUES (35, 287, '2019-10-26 11:19:49');
INSERT INTO `login` VALUES (36, 287, '2019-10-26 11:20:54');
INSERT INTO `login` VALUES (37, 288, '2019-10-26 11:22:58');
INSERT INTO `login` VALUES (38, 288, '2019-10-26 11:26:56');
INSERT INTO `login` VALUES (39, 288, '2019-10-26 11:27:57');
INSERT INTO `login` VALUES (40, 288, '2019-10-26 11:29:15');
INSERT INTO `login` VALUES (41, 288, '2019-10-26 11:33:49');
INSERT INTO `login` VALUES (42, 288, '2019-10-26 11:35:24');
INSERT INTO `login` VALUES (43, 288, '2019-10-26 11:38:12');
INSERT INTO `login` VALUES (44, 288, '2019-10-26 11:43:56');
INSERT INTO `login` VALUES (45, 288, '2019-10-26 13:57:03');
INSERT INTO `login` VALUES (46, 288, '2019-10-26 14:04:53');
INSERT INTO `login` VALUES (47, 288, '2019-10-26 14:21:30');
INSERT INTO `login` VALUES (48, 288, '2019-10-26 14:24:41');
INSERT INTO `login` VALUES (49, 288, '2019-10-26 14:27:20');
INSERT INTO `login` VALUES (50, 288, '2019-10-26 14:34:07');
INSERT INTO `login` VALUES (51, 288, '2019-10-26 14:48:10');
INSERT INTO `login` VALUES (52, 288, '2019-10-26 14:53:31');
INSERT INTO `login` VALUES (53, 288, '2019-10-26 14:55:20');
INSERT INTO `login` VALUES (54, 288, '2019-10-26 15:07:12');
INSERT INTO `login` VALUES (55, 288, '2019-10-26 15:20:49');
INSERT INTO `login` VALUES (56, 288, '2019-10-26 15:52:08');
INSERT INTO `login` VALUES (57, 288, '2019-10-26 15:53:52');
INSERT INTO `login` VALUES (58, 128, '2019-10-26 15:56:59');
INSERT INTO `login` VALUES (59, 128, '2019-10-26 16:27:18');
INSERT INTO `login` VALUES (60, 128, '2019-10-26 16:36:10');
INSERT INTO `login` VALUES (61, 128, '2019-10-26 16:46:44');
INSERT INTO `login` VALUES (62, 128, '2019-10-26 16:58:54');
INSERT INTO `login` VALUES (63, 128, '2019-10-26 17:06:58');
INSERT INTO `login` VALUES (64, 128, '2019-10-26 17:08:48');
INSERT INTO `login` VALUES (65, 128, '2019-10-26 17:09:55');
INSERT INTO `login` VALUES (66, 128, '2019-10-26 17:18:26');
INSERT INTO `login` VALUES (67, 128, '2019-10-26 17:27:06');
INSERT INTO `login` VALUES (68, 119, '2019-10-23 17:12:53');
INSERT INTO `login` VALUES (69, 120, '2019-10-23 18:36:46');
INSERT INTO `login` VALUES (70, 2, '2019-10-29 13:31:32');
INSERT INTO `login` VALUES (71, 2, '2019-10-29 14:20:40');
INSERT INTO `login` VALUES (72, 128, '2019-10-29 14:24:04');
INSERT INTO `login` VALUES (73, 128, '2019-10-29 14:27:43');
INSERT INTO `login` VALUES (74, 128, '2019-10-29 14:29:32');
INSERT INTO `login` VALUES (75, 128, '2019-10-29 14:30:50');
INSERT INTO `login` VALUES (76, 128, '2019-10-29 14:30:58');
INSERT INTO `login` VALUES (77, 128, '2019-10-29 14:34:44');
INSERT INTO `login` VALUES (78, 123, '2019-10-29 14:39:22');
INSERT INTO `login` VALUES (79, 123, '2019-10-29 14:42:02');
INSERT INTO `login` VALUES (80, 123, '2019-10-29 14:43:44');
INSERT INTO `login` VALUES (81, 128, '2019-10-29 14:52:05');
INSERT INTO `login` VALUES (82, 2, '2019-10-29 14:52:41');
INSERT INTO `login` VALUES (83, 2, '2019-10-29 14:53:28');
INSERT INTO `login` VALUES (84, 2, '2019-10-29 14:56:50');
INSERT INTO `login` VALUES (85, 2, '2019-10-29 14:57:50');
INSERT INTO `login` VALUES (86, 189, '2019-10-29 14:58:38');
INSERT INTO `login` VALUES (87, 189, '2019-10-29 15:00:16');
INSERT INTO `login` VALUES (88, 189, '2019-10-29 15:05:42');
INSERT INTO `login` VALUES (89, 189, '2019-10-29 15:12:34');
INSERT INTO `login` VALUES (90, 189, '2019-10-29 15:17:14');
INSERT INTO `login` VALUES (91, 192, '2019-10-29 15:30:21');
INSERT INTO `login` VALUES (92, 192, '2019-10-29 15:40:52');
INSERT INTO `login` VALUES (93, 192, '2019-10-29 15:52:52');
INSERT INTO `login` VALUES (94, 128, '2019-10-29 16:33:12');
INSERT INTO `login` VALUES (95, 128, '2019-10-29 16:37:01');
INSERT INTO `login` VALUES (96, 4, '2019-10-29 16:43:51');
INSERT INTO `login` VALUES (97, 287, '2019-10-29 16:44:33');
INSERT INTO `login` VALUES (98, 287, '2019-10-29 16:47:17');
INSERT INTO `login` VALUES (99, 287, '2019-10-29 16:57:31');
INSERT INTO `login` VALUES (100, 287, '2019-10-29 17:01:11');
INSERT INTO `login` VALUES (101, 128, '2019-10-29 17:02:55');
INSERT INTO `login` VALUES (102, 128, '2019-10-29 17:03:32');
INSERT INTO `login` VALUES (103, 128, '2019-10-29 17:17:25');
INSERT INTO `login` VALUES (104, 2, '2019-10-29 17:18:22');
INSERT INTO `login` VALUES (105, 2, '2019-10-29 17:18:50');
INSERT INTO `login` VALUES (106, 2, '2019-10-29 17:38:45');
INSERT INTO `login` VALUES (107, 128, '2019-10-29 17:40:16');
INSERT INTO `login` VALUES (108, 128, '2019-10-29 17:54:31');
INSERT INTO `login` VALUES (109, 128, '2019-10-29 19:06:06');
INSERT INTO `login` VALUES (110, 128, '2019-10-29 19:11:40');
INSERT INTO `login` VALUES (111, 128, '2019-10-29 19:15:48');
INSERT INTO `login` VALUES (112, 2, '2019-10-29 19:20:02');
INSERT INTO `login` VALUES (113, 2, '2019-10-29 20:14:23');
INSERT INTO `login` VALUES (114, 2, '2019-10-29 20:37:06');
INSERT INTO `login` VALUES (115, 2, '2019-10-29 20:42:54');
INSERT INTO `login` VALUES (116, 2, '2019-10-29 20:49:01');
INSERT INTO `login` VALUES (117, 2, '2019-10-29 20:55:42');
INSERT INTO `login` VALUES (118, 2, '2019-10-29 21:01:13');
INSERT INTO `login` VALUES (119, 128, '2019-10-29 21:06:46');
INSERT INTO `login` VALUES (120, 128, '2019-10-29 22:32:28');
INSERT INTO `login` VALUES (121, 128, '2019-10-29 22:44:16');
INSERT INTO `login` VALUES (122, 128, '2019-10-29 23:16:23');
INSERT INTO `login` VALUES (123, 128, '2019-10-30 09:03:20');
INSERT INTO `login` VALUES (124, 128, '2019-10-30 09:05:25');
INSERT INTO `login` VALUES (125, 128, '2019-10-30 10:39:13');
INSERT INTO `login` VALUES (126, 128, '2019-10-30 10:48:17');
INSERT INTO `login` VALUES (127, 128, '2019-10-30 11:49:33');
INSERT INTO `login` VALUES (128, 128, '2019-10-30 14:07:07');
INSERT INTO `login` VALUES (129, 128, '2019-10-30 14:07:20');
INSERT INTO `login` VALUES (130, 2, '2019-10-30 14:19:06');
INSERT INTO `login` VALUES (131, 2, '2019-10-30 14:19:30');
INSERT INTO `login` VALUES (132, 5, '2019-10-30 14:20:17');
INSERT INTO `login` VALUES (133, 128, '2019-10-30 14:20:53');
INSERT INTO `login` VALUES (134, 128, '2019-10-30 14:26:18');
INSERT INTO `login` VALUES (135, 128, '2019-10-30 15:13:10');
INSERT INTO `login` VALUES (136, 128, '2019-10-30 15:14:22');
INSERT INTO `login` VALUES (137, 128, '2019-10-30 15:30:53');
INSERT INTO `login` VALUES (138, 2, '2019-10-30 16:25:50');
INSERT INTO `login` VALUES (139, 2, '2019-10-30 18:59:40');
INSERT INTO `login` VALUES (140, 2, '2019-10-30 19:11:58');
INSERT INTO `login` VALUES (141, 2, '2019-10-30 19:21:16');
INSERT INTO `login` VALUES (142, 2, '2019-10-30 19:21:42');
INSERT INTO `login` VALUES (143, 5, '2019-10-30 19:23:00');
INSERT INTO `login` VALUES (144, 259, '2019-10-30 19:31:01');
INSERT INTO `login` VALUES (145, 259, '2019-10-30 20:13:41');
INSERT INTO `login` VALUES (146, 132, '2019-10-30 20:17:42');
INSERT INTO `login` VALUES (147, 293, '2019-10-30 20:27:27');
INSERT INTO `login` VALUES (148, 293, '2019-10-30 20:34:46');
INSERT INTO `login` VALUES (149, 293, '2019-10-30 20:40:29');
INSERT INTO `login` VALUES (150, 263, '2019-10-30 20:52:32');
INSERT INTO `login` VALUES (151, 293, '2019-10-30 21:02:59');
INSERT INTO `login` VALUES (152, 293, '2019-10-30 21:04:54');
INSERT INTO `login` VALUES (153, 291, '2019-10-30 21:59:51');
INSERT INTO `login` VALUES (154, 291, '2019-10-30 22:07:41');
INSERT INTO `login` VALUES (155, 291, '2019-10-30 22:12:43');
INSERT INTO `login` VALUES (156, 291, '2019-10-30 22:42:07');
INSERT INTO `login` VALUES (157, 2, '2019-10-30 22:49:52');

-- ----------------------------
-- Table structure for operation
-- ----------------------------
DROP TABLE IF EXISTS `operation`;
CREATE TABLE `operation`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '操作ID',
  `userid` int(11) NULL DEFAULT NULL COMMENT '员工ID',
  `type` int(11) NOT NULL COMMENT '操作类型 1.添加 2.删除 3.修改 4.查询',
  `operationTime` datetime(0) NULL DEFAULT NULL COMMENT '操作时间',
  `adminid` int(5) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 171 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operation
-- ----------------------------
INSERT INTO `operation` VALUES (1, 2, 4, '2019-10-30 21:01:17', 263);
INSERT INTO `operation` VALUES (2, 2, 3, '2019-10-30 21:01:26', 263);
INSERT INTO `operation` VALUES (3, 132, 4, '2019-10-30 21:03:04', 293);
INSERT INTO `operation` VALUES (4, 256, 4, '2019-10-30 21:03:11', 293);

-- ----------------------------
-- Table structure for operationtype
-- ----------------------------
DROP TABLE IF EXISTS `operationtype`;
CREATE TABLE `operationtype`  (
  `id` int(5) NOT NULL,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of operationtype
-- ----------------------------
INSERT INTO `operationtype` VALUES (1, '添加');
INSERT INTO `operationtype` VALUES (2, '删除');
INSERT INTO `operationtype` VALUES (3, '修改');
INSERT INTO `operationtype` VALUES (4, '查询');

-- ----------------------------
-- Table structure for position
-- ----------------------------
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position`  (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '职位ID',
  `departmentId` int(10) NULL DEFAULT NULL COMMENT '部门id',
  `positionName` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '职位名称',
  `planNumber` int(10) NULL DEFAULT NULL COMMENT '计划招聘人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of position
-- ----------------------------
INSERT INTO `position` VALUES (1, 1, '设计师', 10);
INSERT INTO `position` VALUES (2, 2, '技术总监', 5);
INSERT INTO `position` VALUES (3, 3, '经理', 2);
INSERT INTO `position` VALUES (4, 4, '主管', 10);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '员工ID',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '密码',
  `sex` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别 1.男 2.女',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户姓名',
  `phone` bigint(11) NOT NULL COMMENT '电话号码',
  `qq` bigint(20) NULL DEFAULT NULL COMMENT 'QQ',
  `UID` bigint(20) NOT NULL COMMENT '身份证号',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱',
  `headimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT 'img/pic11.jpg' COMMENT '头像',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `departmentId` int(255) NOT NULL COMMENT '部门ID',
  `positionId` int(255) NOT NULL COMMENT '职位ID 1.主管 2.设计师 3.经理 4.设计总监',
  `entryTime` datetime(0) NULL DEFAULT NULL COMMENT '入职时间',
  `mark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `IsAdmin` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '是否是管理员',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `phone`(`phone`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 291 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (2, '123456', '女', 'user002', 15225304980, 2011533705, 411526199804184810, 'shiyong@gmail.com', 'img/pic11.jpg', '北京市朝阳区', 3, 1, '2019-10-03 00:00:00', NULL, '0');
INSERT INTO `user` VALUES (4, '123456', '女', '张三分', 13113131313, 22222222, 422222222266664444, 'zhangsanfen@qq.com', 'img/main_02.jpg', '北京市朝阳区', 3, 2, '2019-07-31 00:00:00', NULL, '0');
INSERT INTO `user` VALUES (5, '123456', '女', '张二飒', 12319651, 1645161541, 1654102417, '2651654121', 'img/main_03.jpg', '郑州', 4, 1, '2019-10-22 00:00:00', NULL, '1');
INSERT INTO `user` VALUES (132, '123456', '男', '张无忌', 36851258960, 2011533705, 189451615, '65412345123', 'img/main_03.jpg', '郑州', 1, 2, '2019-10-25 00:00:00', NULL, '1');
INSERT INTO `user` VALUES (191, '123456', '男', '测试9', 13122222227, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic06.jpg', '郑州', 1, 1, '2019-10-25 00:00:00', NULL, '0');
INSERT INTO `user` VALUES (256, '123456', '男', '测试5', 124122222223, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic09.jpg', '郑州', 2, 2, '2019-10-25 00:00:00', NULL, '0');
INSERT INTO `user` VALUES (257, '123456', '男', '测试6', 12542222224, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic10.jpg', '郑州', 3, 3, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (258, '123456', '男', '测试7', 1242222225, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic11.jpg', '郑州', 3, 1, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (259, '123456', '男', '测试8', 13222222226, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic01.jpg', '郑州', 3, 2, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (260, '123456', '男', '测试9', 1325222227, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic02.jpg', '郑州', 3, 4, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (261, '123456', '男', '测试10', 125122222228, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic03.jpg', '郑州', 3, 1, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (262, '123456', '男', '测试11', 132522222229, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic04.jpg', '郑州', 3, 4, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (263, '123456', '男', '测试12', 13122222220, 2011533705, 5916651911919546, '26545@qq.com', 'img/pic05.jpg', '郑州', 3, 2, '2019-10-25 23:52:30', NULL, '0');
INSERT INTO `user` VALUES (291, '123456', '男', '测试1', 19878561245, 19878561245, 496512562, '465261616@qq.com', 'img/pic01.jpg', '郑州', 3, 2, '2019-10-30 14:08:38', NULL, '1');
INSERT INTO `user` VALUES (292, '123456', '男', '测试2', 984659465, 984659465, 496598465, '849654965@qq.com', 'img/pic01.jpg', '郑州', 2, 4, '2019-10-30 16:22:34', NULL, '1');
INSERT INTO `user` VALUES (293, '123456', '男', '测试3', 13256897845, 13256897845, 798465798465, '4984123@qq.com', 'img/2019_10_30_83227_pic10.jpg', '495648956', 3, 3, '2019-10-30 16:23:12', NULL, '1');

SET FOREIGN_KEY_CHECKS = 1;
